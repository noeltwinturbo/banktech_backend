package mongoConfig

import (
	"banktech_backend/utils/constants"
	"time"

	"go.mongodb.org/mongo-driver/mongo/options"
)

//Returns the mongo config
func GetMongoConnOptions() *options.ClientOptions {
	connOptions := options.Client()
	connOptions.SetAppName(constants.APP_NAME)
	connOptions.SetConnectTimeout(constants.MONGO_TIMEOUT * time.Second)
	connOptions.SetSocketTimeout(constants.MONGO_TIMEOUT * time.Second)
	connOptions.SetServerSelectionTimeout(constants.MONGO_TIMEOUT * time.Second)
	auth := options.Credential{
		AuthMechanism: constants.MONGO_AUTH_MECHANISM,
		AuthSource:    constants.MONGO_DATABASE,
		Username:      constants.MONGO_USER,
		Password:      constants.MONGO_PASSWORD,
	}
	connOptions.SetAuth(auth)
	connOptions.ApplyURI(constants.MONGO_URI)
	return connOptions
}
