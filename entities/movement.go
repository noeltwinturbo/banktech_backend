package entities

import "time"

type Movement struct {
	MovementId    int64     `json:"movementId,omitempty" bson:"movementId"`
	Type          int       `json:"type" bson:"type" validate:"required,movementType"`
	Amount        float32   `json:"amount" bson:"amount" validate:"required,gt=0,lte=1000"`
	SenderIban    string    `json:"senderIban" bson:"senderIban"`
	RecipientIban string    `json:"recipientIban" bson:"recipientIban" validate:"required,iban"`
	Date          time.Time `json:"date" bson:"date"`
	Concept       string    `json:"concept" bson:"concept" validate:"required,min=3,max=50"`
	IsCanceled    bool      `json:"isCanceled" bson:"isCanceled"`
}
