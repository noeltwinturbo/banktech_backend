package models

import (
	"banktech_backend/configs/mongoConfig"
	"banktech_backend/utils/constants"
	"banktech_backend/utils/constants/envConfigConstants"
	"banktech_backend/utils/constants/fieldConstants"
	"banktech_backend/utils/loggerUtils"
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

type ModelHelper struct {
}

//Opens a mongoDB connection
func openConnection() (*mongo.Client, error) {
	connOptions := mongoConfig.GetMongoConnOptions()

	client, err := mongo.NewClient(connOptions)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al configurar la conexión con la BBDD. Error: %s", err)
		return nil, err
	}

	err = client.Connect(context.TODO())
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al intentar abrir la conexión con la BBDD. Error: %s", err)
		return nil, err
	}

	return client, nil
}

//Closes a mongoDB connection
func closeConnection(client *mongo.Client) {
	err := client.Disconnect(context.TODO())
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al intentar cerrar la conexión con la BBDD. Error: %s", err)
	}
}

//Generic insert entity into the DDBB
func insertEntity(coll string, entity interface{}) (*mongo.InsertOneResult, error) {
	client, err := openConnection()
	if err != nil {
		return nil, err
	}

	collection := client.Database(constants.MONGO_DATABASE).Collection(coll)
	result, err := collection.InsertOne(context.TODO(), entity)
	if err != nil || result.InsertedID == nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error a la hora de insertar el documento %s en la colección %s. Error: %s", entity, coll, err)
		return nil, err
	}

	defer closeConnection(client)

	return result, nil
}

//Generic update entity into the DDBB
func updateEntityByFields(coll string, filter interface{}, update interface{}) (int64, error) {
	client, err := openConnection()
	if err != nil {
		return 0, err
	}

	collection := client.Database(constants.MONGO_DATABASE).Collection(coll)
	updatedResult, err := collection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error a la hora de actualizar el documento con el filtro %s en la colección %s. Error: %s", filter, coll, err)
		return 0, err
	}

	defer closeConnection(client)

	return updatedResult.ModifiedCount, nil
}

//Generic update entities into the DDBB
func updateEntitiesByFields(coll string, filter interface{}, update interface{}) (int64, error) {
	client, err := openConnection()
	if err != nil {
		return 0, err
	}

	collection := client.Database(constants.MONGO_DATABASE).Collection(coll)
	updatedResult, err := collection.UpdateMany(context.TODO(), filter, update)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error a la hora de actualizar los documentos con el filtro %s en la colección %s. Error: %s", filter, coll, err)
		return 0, err
	}

	defer closeConnection(client)

	return updatedResult.ModifiedCount, nil
}

//Generic delete entity by fields in the DDBB
func deleteEntityByFields(coll string, filter interface{}) (int64, error) {
	client, err := openConnection()
	if err != nil {
		return 0, err
	}

	collection := client.Database(constants.MONGO_DATABASE).Collection(coll)
	deletedResult, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error a la hora de eliminar el documento con el filtro %s en la colección %s. Error: %s", filter, coll, err)
		return 0, err
	}

	defer closeConnection(client)

	return deletedResult.DeletedCount, nil
}

//Generic find entity by fields in the DDBB
func findEntityByFields(coll string, filter interface{}) (*mongo.SingleResult, error) {
	client, err := openConnection()
	if err != nil {
		return nil, err
	}

	collection := client.Database(constants.MONGO_DATABASE).Collection(coll)
	result := collection.FindOne(context.TODO(), filter)
	if result.Err() != nil {
		if result.Err() == mongo.ErrNoDocuments {
			if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
				loggerUtils.LOGGER_FINE.Printf("No se ha encontrado ningún documento en la colección %s con el filtro %s en la BBDD", coll, filter)
			}
			return nil, nil
		} else {
			loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al buscar un documento en la colección %s con el filtro %s en la BBDD. Error: %s", coll, filter, err)
			return nil, err
		}
	}

	defer closeConnection(client)

	return result, nil
}

//Generic find entities by fields in the DDBB
func findEntitiesByFields(coll string, filter interface{}) (*mongo.Cursor, error) {
	client, err := openConnection()
	if err != nil {
		return nil, err
	}

	collection := client.Database(constants.MONGO_DATABASE).Collection(coll)
	result, err := collection.Find(context.TODO(), filter)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
				loggerUtils.LOGGER_FINE.Printf("No se ha encontrado ningún documento en la colección %s con el filtro %s en la BBDD", coll, filter)
			}
			return nil, nil
		} else {
			loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al buscar en la colección %s con el filtro %s en la BBDD. Error: %s", coll, filter, err)
			return nil, err
		}
	}

	defer closeConnection(client)

	return result, nil
}

//Generic count entities by fields in the DDBB
func countEntitiesByFields(coll string, filter interface{}) (int64, error) {
	client, err := openConnection()
	if err != nil {
		return 0, err
	}

	collection := client.Database(constants.MONGO_DATABASE).Collection(coll)
	result, err := collection.CountDocuments(context.TODO(), filter)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error a la contar los documentos de la colección %s con el filetro filtro %s en la BBDD. Error: %s", coll, filter, err)
		return 0, err
	}

	defer closeConnection(client)

	return result, nil
}

//Checks if DDBB has all configs, else creates it
func (this ModelHelper) CheckDataBaseStatus() {
	loggerUtils.LOGGER_INFO.Printf("Comprobando si la BBDD %s está configurada", constants.MONGO_DATABASE)

	client, err := openConnection()
	if err != nil {
		panic(err)
	}

	createDBUniqueIndex(client)

	if !checkIfCollExists(constants.COLL_USERS, client) {
		createAdminUser()
	}

	if !checkIfCollExists(constants.COLL_IDENTIFIERS, client) {
		createZeroIdentifiers()
	}

	var envConfigsModel EnvConfigsModel
	err = envConfigsModel.ReloadCacheEnvConfigs()
	if err != nil {
		panic(err)
	}

	defer closeConnection(client)
}

//Creates Unique Index in all collections if it does not exist
func createDBUniqueIndex(client *mongo.Client) {
	if !checkIfCollExists(constants.COLL_ENV_CONFIGS, client) {
		createEnvConfigsUniqueIndex(client)
	}
	if !checkIfCollExists(constants.COLL_ACCOUNTS, client) {
		createAccountsUniqueIndex(client)
	}
}

//Checks if collection exists
func checkIfCollExists(coll string, client *mongo.Client) bool {
	toret := false

	collectionNames, err := client.Database(constants.MONGO_DATABASE).ListCollectionNames(context.TODO(), bson.D{})
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al listar las colecciones de la BBDD %s", constants.MONGO_DATABASE)
		panic(err)
	}

	for _, collectionName := range collectionNames {
		if collectionName == coll {
			toret = true
			break
		}
	}

	return toret
}

//Creates EnvConfigs Unique Index collection
func createEnvConfigsUniqueIndex(client *mongo.Client) {
	loggerUtils.LOGGER_INFO.Printf("Creando los Unique Index para la colección %s", constants.COLL_ENV_CONFIGS)

	collection := client.Database(constants.MONGO_DATABASE).Collection(constants.COLL_ENV_CONFIGS)
	indexes := []mongo.IndexModel{
		{
			Keys:    bsonx.Doc{{Key: fieldConstants.FIELD_ENV_CONFIG_NAME, Value: bsonx.Int32(1)}},
			Options: options.Index().SetUnique(true),
		},
	}

	_, err := collection.Indexes().CreateMany(context.TODO(), indexes)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al crear los Unique Index de la colección %s", constants.COLL_ENV_CONFIGS)
		panic(err)
	}
}

//Creates Accounts Unique Index collection
func createAccountsUniqueIndex(client *mongo.Client) {
	loggerUtils.LOGGER_INFO.Printf("Creando los Unique Index para la colección %s", constants.COLL_ACCOUNTS)

	collection := client.Database(constants.MONGO_DATABASE).Collection(constants.COLL_ACCOUNTS)
	indexes := []mongo.IndexModel{
		{
			Keys:    bsonx.Doc{{Key: fieldConstants.FIELD_ACCOUNT_IBAN, Value: bsonx.Int32(1)}},
			Options: options.Index().SetUnique(true),
		},
	}

	_, err := collection.Indexes().CreateMany(context.TODO(), indexes)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al crear los Unique Index de la colección %s", constants.COLL_ACCOUNTS)
		panic(err)
	}
}
