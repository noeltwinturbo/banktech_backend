package handles

import (
	"banktech_backend/entities"
	"banktech_backend/models"
	"banktech_backend/utils/constants/envConfigConstants"
	"banktech_backend/utils/constants/mailConstants"
	"banktech_backend/utils/constants/messageConstants"
	"banktech_backend/utils/cryptUtils"
	"banktech_backend/utils/loggerUtils"
	"banktech_backend/utils/mailUtils"
	"banktech_backend/utils/restUtils"
	"banktech_backend/utils/tokenUtils"
	"banktech_backend/validatorsEngine"
	"banktech_backend/viewEntities"
	"encoding/json"
	"fmt"
	"net/http"
)

//Do /users/login POST actions
func PostUsersLogin(w http.ResponseWriter, r *http.Request) {
	var login entities.Login
	var userModel models.UsersModel
	var validatorEngine validatorsEngine.ValidatorEngine

	err := json.NewDecoder(r.Body).Decode(&login)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al deserializar el body de la petición. Error: %s", err)
		restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
	} else {
		if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
			input, _ := json.Marshal(&login)
			loggerUtils.LOGGER_FINE.Printf("Body de entrada: %s", input)
		}
		err = validatorEngine.ValidateStruct(&login)
		if err != nil {
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, err.Error())
		} else {
			user, err := userModel.FindUserByEmail(login.Email)
			if err != nil {
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			} else if user == nil {
				restUtils.SetOKOResponseMessage(w, http.StatusUnauthorized, fmt.Sprintf(messageConstants.REST_LOGIN_USER_NOT_FOUND, login.Email))
			} else if login.Password != user.Password {
				restUtils.SetOKOResponseMessage(w, http.StatusUnauthorized, messageConstants.REST_LOGIN_WRONG_PASSWORD)
			} else {
				token, err := tokenUtils.GenerateToken(user)
				if err != nil {
					restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
				} else {
					user.Password = "***"
					http.SetCookie(w, token)
					restUtils.SetOKResponseBody(w, http.StatusOK, user)
				}
			}
		}
	}
}

//Do /users/logout POST actions
func PostUsersLogout(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, tokenUtils.ExpireCurrentToken())
	restUtils.SetEmptyResponse(w, http.StatusOK)
}

//Do /users GET actions
func GetUsers(w http.ResponseWriter, r *http.Request) {
	var userModel models.UsersModel

	userId, _ := tokenUtils.ValidateToken(w, r)
	if userId != nil {
		user, err := userModel.FindUserById(*userId)
		if err != nil || user == nil {
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
		} else {
			restUtils.SetOKResponseBody(w, http.StatusOK, user)
		}
	}
}

//Do /users POST actions
func PostUsers(w http.ResponseWriter, r *http.Request) {
	var user entities.User
	var userModel models.UsersModel
	var usersValidator validatorsEngine.UsersValidator

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al deserializar el body de la petición. Error: %s", err)
		restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
	} else {
		if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
			input, _ := json.Marshal(&user)
			loggerUtils.LOGGER_FINE.Printf("Body de entrada: %s", input)
		}
		err := usersValidator.ValidateInsertUser(&user)
		if err != nil {
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, err.Error())
		} else {
			newUserId, err := userModel.InsertUser(&user)
			if err != nil {
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			} else {
				user.UserId = *newUserId
				user.Password = "***"
				token, err := tokenUtils.GenerateToken(&user)
				if err != nil {
					restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
				} else {
					mailUtils.SendMail(user.Email, fmt.Sprintf(mailConstants.EMAIL_USER_CREATED, user.Name))
					http.SetCookie(w, token)
					restUtils.SetOKResponseBody(w, http.StatusOK, user)
				}
			}
		}
	}
}

//Do /users/data PUT actions
func PutUsersData(w http.ResponseWriter, r *http.Request) {
	var userDataView viewEntities.UserDataView
	var userModel models.UsersModel
	var validatorEngine validatorsEngine.ValidatorEngine

	userId, _ := tokenUtils.ValidateToken(w, r)
	if userId != nil {
		err := json.NewDecoder(r.Body).Decode(&userDataView)
		if err != nil {
			loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al deserializar el body de la petición. Error: %s", err)
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
		} else {
			if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
				input, _ := json.Marshal(&userDataView)
				loggerUtils.LOGGER_FINE.Printf("Body de entrada: %s", input)
			}
			err = validatorEngine.ValidateStruct(&userDataView)
			if err != nil {
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, err.Error())
			} else {
				updated, err := userModel.UpdateUserData(*userId, &userDataView)
				if err != nil || updated == 0 {
					restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
				} else {
					restUtils.SetOKOResponseMessage(w, http.StatusOK, messageConstants.REST_USER_DATA_UPDATED)
				}
			}
		}
	}
}

//Do /users/dni PUT actions
func PutUsersDni(w http.ResponseWriter, r *http.Request) {
	var dniView viewEntities.DniView
	var userModel models.UsersModel
	var usersValidators validatorsEngine.UsersValidator

	userId, isAdmin := tokenUtils.ValidateToken(w, r)
	if userId != nil {
		if *isAdmin {
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_USER_ADMIN_UPDATE)
		} else {
			err := json.NewDecoder(r.Body).Decode(&dniView)
			if err != nil {
				loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al deserializar el body de la petición. Error: %s", err)
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			} else {
				if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
					input, _ := json.Marshal(&dniView)
					loggerUtils.LOGGER_FINE.Printf("Body de entrada: %s", input)
				}
				err = usersValidators.ValidateUpdateUserDni(&dniView)
				if err != nil {
					restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, err.Error())
				} else {
					updated, err := userModel.UpdateUserDni(*userId, dniView.Dni)
					if err != nil || updated == 0 {
						restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
					} else {
						restUtils.SetOKOResponseMessage(w, http.StatusOK, messageConstants.REST_USER_DNI_UPDATED)
					}
				}
			}
		}
	}
}

//Do /users/email PUT actions
func PutUsersEmail(w http.ResponseWriter, r *http.Request) {
	var emailView viewEntities.EmailView
	var userModel models.UsersModel
	var usersValidators validatorsEngine.UsersValidator

	userId, isAdmin := tokenUtils.ValidateToken(w, r)
	if userId != nil {
		if *isAdmin {
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_USER_ADMIN_UPDATE)
		} else {
			err := json.NewDecoder(r.Body).Decode(&emailView)
			if err != nil {
				loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al deserializar el body de la petición. Error: %s", err)
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			} else {
				if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
					input, _ := json.Marshal(&emailView)
					loggerUtils.LOGGER_FINE.Printf("Body de entrada: %s", input)
				}
				err = usersValidators.ValidateUpdateUserEmail(&emailView)
				if err != nil {
					restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, err.Error())
				} else {
					updated, err := userModel.UpdateUserEmail(*userId, emailView.Email)
					if err != nil || updated == 0 {
						restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
					} else {
						restUtils.SetOKOResponseMessage(w, http.StatusOK, messageConstants.REST_USER_EMAIL_UPDATED)
					}
				}
			}
		}
	}
}

//Do /users/password PUT actions
func PutUsersPassword(w http.ResponseWriter, r *http.Request) {
	var passwordView viewEntities.PasswordView
	var userModel models.UsersModel
	var validatorsEngine validatorsEngine.ValidatorEngine

	userId, _ := tokenUtils.ValidateToken(w, r)
	if userId != nil {
		err := json.NewDecoder(r.Body).Decode(&passwordView)
		if err != nil {
			loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al deserializar el body de la petición. Error: %s", err)
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
		} else {
			if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
				input, _ := json.Marshal(&passwordView)
				loggerUtils.LOGGER_FINE.Printf("Body de entrada: %s", input)
			}
			err = validatorsEngine.ValidateStruct(&passwordView)
			if err != nil {
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, err.Error())
			} else {
				updated, err := userModel.UpdateUserPassword(*userId, passwordView.Password)
				if err != nil || updated == 0 {
					restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
				} else {
					restUtils.SetOKOResponseMessage(w, http.StatusOK, messageConstants.REST_USER_PASSWORD_UPDATED)
				}
			}
		}
	}
}

//Do /users/config PUT actions
func PutUsersConfig(w http.ResponseWriter, r *http.Request) {
	var userConfigView viewEntities.UserConfigView
	var userModel models.UsersModel
	var validatorEngine validatorsEngine.ValidatorEngine

	userId, _ := tokenUtils.ValidateToken(w, r)
	if userId != nil {
		err := json.NewDecoder(r.Body).Decode(&userConfigView)
		if err != nil {
			loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al deserializar el body de la petición. Error: %s", err)
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
		} else {
			if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
				input, _ := json.Marshal(&userConfigView)
				loggerUtils.LOGGER_FINE.Printf("Body de entrada: %s", input)
			}
			err = validatorEngine.ValidateStruct(userConfigView)
			if err != nil {
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, err.Error())
			} else {
				updated, err := userModel.UpdateUserConfig(*userId, &userConfigView)
				if err != nil || updated == 0 {
					restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
				} else {
					restUtils.SetOKOResponseMessage(w, http.StatusOK, messageConstants.REST_USER_CONFIG_UPDATED)
				}
			}
		}
	}
}

//Do /users DELETE actions
func DeleteUsers(w http.ResponseWriter, r *http.Request) {
	var userModel models.UsersModel

	userId, isAdmin := tokenUtils.ValidateToken(w, r)
	if isAdmin != nil && *isAdmin {
		restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_USER_ADMIN_DELETE)
	} else if userId != nil {
		updated, err := userModel.DeleteUser(*userId)
		if err != nil || updated == 0 {
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
		} else {
			user, _ := userModel.FindUserById(*userId)
			mailUtils.SendMail(user.Email, fmt.Sprintf(mailConstants.EMAIL_USER_DELETED, user.Name))
			http.SetCookie(w, tokenUtils.ExpireCurrentToken())
			restUtils.SetOKOResponseMessage(w, http.StatusOK, messageConstants.REST_USER_DELETED)
		}
	}
}

//Do /users/resetPassword POST actions
func PostUsersResetPassword(w http.ResponseWriter, r *http.Request) {
	var emailView viewEntities.EmailView
	var userModel models.UsersModel
	var validatorEngine validatorsEngine.ValidatorEngine

	err := json.NewDecoder(r.Body).Decode(&emailView)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al deserializar el body de la petición. Error: %s", err)
		restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
	} else {
		if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
			input, _ := json.Marshal(&emailView)
			loggerUtils.LOGGER_FINE.Printf("Body de entrada: %s", input)
		}
		err = validatorEngine.ValidateStruct(emailView)
		if err != nil {
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, err.Error())
		} else {
			user, err := userModel.FindUserByEmail(emailView.Email)
			if err != nil {
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			} else if user == nil {
				restUtils.SetOKOResponseMessage(w, http.StatusUnauthorized, fmt.Sprintf(messageConstants.REST_LOGIN_USER_NOT_FOUND, emailView.Email))
			} else {
				newPassword := cryptUtils.GenerateRandomPassword()
				updated, err := userModel.UpdateUserPassword(user.UserId, newPassword)
				if err != nil || updated == 0 {
					restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
				} else {
					err = mailUtils.SendMail(user.Email, fmt.Sprintf(mailConstants.EMAIL_USER_PASSWORD_RESET, user.Name, newPassword))
					if err != nil {
						restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
					} else {
						restUtils.SetOKOResponseMessage(w, http.StatusOK, messageConstants.REST_USER_PASSWORD_RESET)
					}
				}
			}
		}
	}
}
