package controllers

import (
	"banktech_backend/handles"
	"banktech_backend/utils/loggerUtils"
	"net/http"

	"github.com/gorilla/mux"
)

//Inits Accounts controllers
func loadAccountsControllers(r *mux.Router) {
	loggerUtils.LOGGER_INFO.Printf("Iniciando Accounts Controllers")

	r.HandleFunc("/accounts", handles.GetAccounts).Methods(http.MethodGet)
	r.HandleFunc("/accounts/{accountId:[A-Za-z0-9]+}", handles.GetAccount).Methods(http.MethodGet)
	r.HandleFunc("/accounts", handles.PostAccounts).Methods(http.MethodPost)
	r.HandleFunc("/accounts/{accountId:[A-Za-z0-9]+}", handles.PutAccounts).Methods(http.MethodPut)
	r.HandleFunc("/accounts/{accountId:[A-Za-z0-9]+}", handles.DeleteAccounts).Methods(http.MethodDelete)
	r.HandleFunc("/accounts/{accountId:[A-Za-z0-9]+}/changeOwner", handles.PutAccountChangeOwner).Methods(http.MethodPut)
	r.HandleFunc("/accounts/{accountId:[A-Za-z0-9]+}/movements", handles.GetAccountMovements).Methods(http.MethodGet)
	r.HandleFunc("/accounts/{accountId:[A-Za-z0-9]+}/movements", handles.PostAccountMovements).Methods(http.MethodPost)
	r.HandleFunc("/accounts/{accountId:[A-Za-z0-9]+}/movements/{movementId:[0-9]+}", handles.DeleteAccountMovements).Methods(http.MethodDelete)
}
