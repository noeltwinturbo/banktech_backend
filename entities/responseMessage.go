package entities

import (
	"banktech_backend/utils/loggerUtils"
	"encoding/json"
)

type ResponseMessage struct {
	Message string `json:"message"`
}

//Convert input message into a JSON
func (this ResponseMessage) ToJSON(msg string) string {
	this.Message = msg

	toret, err := json.Marshal(this)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error a la hora de obtener el JSON del objeto ResponseMessage. Error: %s", err)
		return msg
	}

	return string(toret)
}
