package controllers

import (
	"banktech_backend/utils/loggerUtils"

	"github.com/gorilla/mux"
)

//Inits all server controllers
func InitServerControllers(r *mux.Router) {
	loggerUtils.LOGGER_INFO.Printf("Iniciando Controllers del servidor")

	loadUsersControllers(r)
	loadAccountsControllers(r)
	loadEnvConfigsControllers(r)

	loggerUtils.LOGGER_INFO.Printf("Controllers del servidor iniciados")
}
