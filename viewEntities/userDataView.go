package viewEntities

import (
	"banktech_backend/entities"
	"time"
)

type UserDataView struct {
	Name     string           `json:"name" validate:"required,min=3,max=25"`
	LastName string           `json:"lastName" validate:"required,min=3,max=50"`
	Birthday time.Time        `json:"birthday" validate:"required,birthday"`
	Phone    string           `json:"phone" validate:"required,phone"`
	Address  entities.Address `json:"address" validate:"required"`
}
