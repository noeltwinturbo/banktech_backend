package validatorsEngine

import (
	"banktech_backend/entities"
	"banktech_backend/models"
	"banktech_backend/utils/constants"
	"banktech_backend/utils/constants/messageConstants"
	"errors"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type MovementsValidator struct {
}

//Executes the Movements input validation before insert into the DDBB
func (this MovementsValidator) ValidateInsertMovement(movement *entities.Movement, accountId primitive.ObjectID) error {
	var validatorEngine ValidatorEngine
	var identifiersModel models.IdentifiersModel
	var accountsModel models.AccountsModel

	err := validatorEngine.ValidateStruct(movement)
	if err != nil {
		return err
	}

	if movement.Type != constants.MOVEMENT_DEPOSIT && movement.Type != constants.MOVEMENT_WITHDRAWALS && !existsRecipientIban(movement.RecipientIban) {
		return errors.New(fmt.Sprintf(messageConstants.REST_MOVEMENT_RECIPIENT_IBAN_NOT_FOUND, movement.RecipientIban))
	}

	account, _ := accountsModel.FindAccountByAccountId(accountId)
	movement.SenderIban = account.Iban
	if movement.Type == constants.MOVEMENT_DEPOSIT || movement.Type == constants.MOVEMENT_WITHDRAWALS {
		movement.RecipientIban = account.Iban
	}

	if movement.Type != constants.MOVEMENT_DEPOSIT && !hasEnoughAmount(movement.SenderIban, movement.Amount) {
		return errors.New(messageConstants.REST_MOVEMENT_NOT_ENOUGH_AMOUNT)
	}

	movementId, err := identifiersModel.GetNewMovementIdentifier()
	if err != nil {
		return errors.New(messageConstants.REST_GENERIC_ERROR)
	}
	movement.MovementId = *movementId

	if movement.Type == constants.MOVEMENT_DEPOSIT {
		movement.Concept = constants.MOVEMENT_DEPOSIT_TEXT
	} else if movement.Type == constants.MOVEMENT_WITHDRAWALS {
		movement.Concept = constants.MOVEMENT_WITHDRAWALS_TEXT
	}

	movement.Date = time.Now()
	movement.IsCanceled = false

	return nil
}

//Checks if recipient IBAN exists
func existsRecipientIban(recipientIban string) bool {
	var accountsModel models.AccountsModel

	return accountsModel.CountAccountsByIban(recipientIban) > 0
}

//Checks if the account has enough amount
func hasEnoughAmount(senderIban string, movementAmount float32) bool {
	var accountsModel models.AccountsModel

	return accountsModel.GetTotalAmount(senderIban) > movementAmount
}
