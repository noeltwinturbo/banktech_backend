package entities

import "go.mongodb.org/mongo-driver/bson/primitive"

type Identifier struct {
	IdentifierId primitive.ObjectID `bson:"_id"`
	MovementId   int64              `bson:"movementId"`
}
