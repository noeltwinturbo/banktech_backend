package viewEntities

type UserConfigView struct {
	IsGeneralNotifyEnable   bool  `json:"isGeneralNotifyEnable"`
	IsMovementsNotifyEnable bool  `json:"isMovementsNotifyEnable"`
	MinMovAmntNotify        int64 `json:"minMovAmntNotify" validate:"gte=0,lte=1000"`
}
