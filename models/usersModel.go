package models

import (
	"banktech_backend/entities"
	"banktech_backend/utils/constants"
	"banktech_backend/utils/constants/fieldConstants"
	"banktech_backend/utils/cryptUtils"
	"banktech_backend/utils/loggerUtils"
	"banktech_backend/viewEntities"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UsersModel struct {
}

//Finds an user by userId
func (this UsersModel) FindUserById(userId primitive.ObjectID) (*entities.User, error) {
	var user *entities.User

	result, err := findEntityByFields(constants.COLL_USERS, bson.M{fieldConstants.FIELD_ID: userId})
	if err != nil || result == nil {
		return nil, err
	}

	err = result.Decode(&user)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al decodificar el resultado de obtener el usario con id %s en la BBDD. Error: %s", userId, err)
		return nil, err
	}

	password, err := cryptUtils.Decrypt(user.Password)
	if err != nil {
		return nil, err
	}

	user.Password = *password

	return user, nil
}

//Finds an user by email
func (this UsersModel) FindUserByEmail(email string) (*entities.User, error) {
	var user *entities.User

	result, err := findEntityByFields(constants.COLL_USERS, bson.M{fieldConstants.FIELD_USER_EMAIL: email, fieldConstants.FIELD_USER_IS_DELETED: false})
	if err != nil || result == nil {
		return nil, err
	}

	err = result.Decode(&user)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al decodificar el resultado de obtener el usario con el email %s en la BBDD. Error: %s", email, err)
		return nil, err
	}

	password, err := cryptUtils.Decrypt(user.Password)
	if err != nil {
		return nil, err
	}

	user.Password = *password

	return user, nil
}

//Finds an user by dni
func (this UsersModel) FindUserByDni(dni string) (*entities.User, error) {
	var user *entities.User

	result, err := findEntityByFields(constants.COLL_USERS, bson.M{fieldConstants.FIELD_USER_DNI: dni, fieldConstants.FIELD_USER_IS_DELETED: false})
	if err != nil || result == nil {
		return nil, err
	}

	err = result.Decode(&user)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al decodificar el resultado de obtener el usario con el dni %s en la BBDD. Error: %s", dni, err)
		return nil, err
	}

	user.Password = "***"

	return user, nil
}

//Counts users by email
func (this UsersModel) CountUsersByEmail(email string) (int64, error) {
	result, err := countEntitiesByFields(constants.COLL_USERS, bson.M{fieldConstants.FIELD_USER_EMAIL: email, fieldConstants.FIELD_USER_IS_DELETED: false})
	if err != nil {
		return result, err
	}

	return result, nil
}

//Counts users by DNI
func (this UsersModel) CountUsersByDNI(dni string) (int64, error) {
	result, err := countEntitiesByFields(constants.COLL_USERS, bson.M{fieldConstants.FIELD_USER_DNI: dni, fieldConstants.FIELD_USER_IS_DELETED: false})
	if err != nil {
		return result, err
	}

	return result, nil
}

//Inserts an user
func (this UsersModel) InsertUser(user *entities.User) (*primitive.ObjectID, error) {
	var accountsModel AccountsModel

	result, err := insertEntity(constants.COLL_USERS, user)
	if err != nil {
		return nil, err
	}

	userId := result.InsertedID.(primitive.ObjectID)

	accountsModel.AssociateAccounts(userId, user.Dni)

	return &userId, nil
}

//Updates the user data
func (this UsersModel) UpdateUserData(userId primitive.ObjectID, userDataView *viewEntities.UserDataView) (int64, error) {
	filter := bson.M{fieldConstants.FIELD_ID: userId}

	update := bson.D{
		{"$set", bson.D{
			{fieldConstants.FIELD_USER_NAME, userDataView.Name},
			{fieldConstants.FIELD_USER_LAST_NAME, userDataView.LastName},
			{fieldConstants.FIELD_USER_BIRTHDAY, userDataView.Birthday},
			{fieldConstants.FIELD_USER_PHONE, userDataView.Phone},
			{fieldConstants.FIELD_USER_ADDRESS, bson.D{
				{fieldConstants.FIELD_ADDRESS_STREET, userDataView.Address.Street},
				{fieldConstants.FIELD_ADDRESS_CITY, userDataView.Address.City},
				{fieldConstants.FIELD_ADDRESS_COUNTRY, userDataView.Address.Country},
				{fieldConstants.FIELD_ADDRESS_ZIPCODE, userDataView.Address.Zipcode},
			}},
		}},
	}

	updated, err := updateEntityByFields(constants.COLL_USERS, filter, update)

	return updated, err
}

//Updates the user data
func (this UsersModel) UpdateUserDni(userId primitive.ObjectID, dni string) (int64, error) {
	var accountsModel AccountsModel

	filter := bson.M{fieldConstants.FIELD_ID: userId}

	update := bson.D{
		{"$set", bson.D{
			{fieldConstants.FIELD_USER_DNI, dni},
		}},
	}

	updated, err := updateEntityByFields(constants.COLL_USERS, filter, update)
	if err != nil || updated == 0 {
		return 0, err
	}

	updated, err = accountsModel.UpdateAccountsDni(userId, dni)

	return updated, err
}

//Updates the user data
func (this UsersModel) UpdateUserEmail(userId primitive.ObjectID, email string) (int64, error) {
	filter := bson.M{fieldConstants.FIELD_ID: userId}

	update := bson.D{
		{"$set", bson.D{
			{fieldConstants.FIELD_USER_EMAIL, email},
		}},
	}

	updated, err := updateEntityByFields(constants.COLL_USERS, filter, update)
	return updated, err
}

//Updates the user config
func (this UsersModel) UpdateUserConfig(userId primitive.ObjectID, userConfigView *viewEntities.UserConfigView) (int64, error) {
	filter := bson.M{fieldConstants.FIELD_ID: userId}

	update := bson.D{
		{"$set", bson.D{
			{fieldConstants.FIELD_USER_USER_CONFIG, bson.D{
				{fieldConstants.FIELD_USER_CONFIG_IS_GENERAL_NOTIFY_ENABLED, userConfigView.IsGeneralNotifyEnable},
				{fieldConstants.FIELD_USER_CONFIG_IS_MOVEMENTS_NOTIFY_ENABLED, userConfigView.IsMovementsNotifyEnable},
				{fieldConstants.FIELD_USER_CONFIG_MIN_MOV_AMNT_NOTIFY, userConfigView.MinMovAmntNotify},
			}},
		}},
	}

	updated, err := updateEntityByFields(constants.COLL_USERS, filter, update)
	return updated, err
}

//Deletes an user
func (this UsersModel) DeleteUser(userId primitive.ObjectID) (int64, error) {
	var accountsModel AccountsModel

	filter := bson.M{fieldConstants.FIELD_ID: userId}

	update := bson.D{
		{"$set", bson.D{
			{fieldConstants.FIELD_USER_IS_DELETED, true},
		}},
	}

	updated, err := updateEntityByFields(constants.COLL_USERS, filter, update)
	if err != nil {
		return updated, err
	}

	accountsModel.DisassociateAccountsByUserId(userId)

	return updated, err
}

//Updates the user password
func (this UsersModel) UpdateUserPassword(userId primitive.ObjectID, newPassword string) (int64, error) {
	encryptedPassword, err := cryptUtils.Encrypt(newPassword)
	if err != nil {
		return 0, err
	}

	filter := bson.M{fieldConstants.FIELD_ID: userId}

	update := bson.D{
		{"$set", bson.D{
			{fieldConstants.FIELD_USER_PASSWORD, encryptedPassword},
		}},
	}

	updated, err := updateEntityByFields(constants.COLL_USERS, filter, update)
	return updated, err
}

//Checks if is general notify enabled
func (this UsersModel) IsGeneralNotifyEnabled(userId primitive.ObjectID) (*string, *string) {
	var user *entities.User

	result, err := findEntityByFields(constants.COLL_USERS, bson.M{fieldConstants.FIELD_ID: userId})
	if err != nil || result == nil {
		return nil, nil
	}

	err = result.Decode(&user)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al decodificar el resultado de obtener el usario con id %s en la BBDD. Error: %s", userId, err)
		return nil, nil
	}

	if user.UserConfig.IsGeneralNotifyEnable {
		return &user.Email, &user.Name
	}

	return nil, nil
}

//Checks if is movements notify enabled
func (this UsersModel) IsMovementsNotifyEnabled(userId primitive.ObjectID, amount float32) (*string, *string) {
	var user *entities.User

	result, err := findEntityByFields(constants.COLL_USERS, bson.M{fieldConstants.FIELD_ID: userId})
	if err != nil || result == nil {
		return nil, nil
	}

	err = result.Decode(&user)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al decodificar el resultado de obtener el usario con id %s en la BBDD. Error: %s", userId, err)
		return nil, nil
	}

	if user.UserConfig.IsMovementsNotifyEnable && amount > float32(user.UserConfig.MinMovAmntNotify) {
		return &user.Email, &user.Name
	}

	return nil, nil
}

//Creates admin user if it does not exist
func createAdminUser() {
	var user entities.User
	var address entities.Address
	var userConfig entities.UserConfig
	var usersModel UsersModel

	loggerUtils.LOGGER_INFO.Printf("Creando el usuario admin")

	user.Name = "Admin"
	user.LastName = "Admin"
	user.Birthday = time.Now()
	user.Phone = "612345667"

	address.Street = "av portugal"
	address.City = "Ourense"
	address.Country = "España"
	address.Zipcode = "32002"
	user.Address = address

	user.Dni = "12345678Z"
	user.Email = "banktechou@gmail.com"
	password, err := cryptUtils.Encrypt(constants.MONGO_PASSWORD)
	if err != nil {
		panic(err)
	}
	user.Password = *password

	userConfig.IsGeneralNotifyEnable = true
	userConfig.IsMovementsNotifyEnable = true
	userConfig.MinMovAmntNotify = 0
	user.UserConfig = userConfig

	user.UserId = primitive.NewObjectID()
	user.IsAdmin = true
	user.IsDeleted = false

	_, err = usersModel.InsertUser(&user)
	if err != nil {
		panic(err)
	}
}
