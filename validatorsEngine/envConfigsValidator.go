package validatorsEngine

import (
	"banktech_backend/entities"
	"banktech_backend/models"
	"banktech_backend/utils/constants/messageConstants"
	"errors"
	"fmt"
)

type EnvConfigsValidator struct {
}

//Executes the EnvConfig input validation before insert into the DDBB
func (this EnvConfigsValidator) ValidateInsertEnvConfig(envConfig *entities.EnvConfig) error {
	var validatorEngine ValidatorEngine

	err := validatorEngine.ValidateStruct(envConfig)
	if err != nil {
		return err
	}

	err = validateIfEnvConfigExists(envConfig.ConfigName)
	if err != nil {
		return err
	}

	return nil
}

//Checks if the EnvConfig exists
func validateIfEnvConfigExists(envConfigName string) error {
	var envConfigsModel models.EnvConfigsModel

	result, err := envConfigsModel.CountEnvConfigsByName(envConfigName)
	if err != nil {
		return errors.New(messageConstants.REST_GENERIC_ERROR)
	} else if result > 0 {
		return errors.New(fmt.Sprintf(messageConstants.REST_ENV_CONFIG_DUPLICATE, envConfigName))
	}

	return nil
}
