package main

import (
	"banktech_backend/configs/serverConfig"
	"banktech_backend/controllers"
	"banktech_backend/models"
	"banktech_backend/utils/constants"
	"banktech_backend/utils/loggerUtils"
	"banktech_backend/utils/restUtils"

	"github.com/gorilla/mux"
)

func main() {
	loggerUtils.InitLogs()

	var modelHelper models.ModelHelper
	modelHelper.CheckDataBaseStatus()

	loggerUtils.LOGGER_INFO.Printf("Iniciando servidor...")

	r := mux.NewRouter().StrictSlash(false)
	r.Use(restUtils.GenerateCommonMiddleware)
	controllers.InitServerControllers(r)
	server := serverConfig.GetServerConfig(r)

	loggerUtils.LOGGER_INFO.Printf("Servidor iniciado en el puerto %s", constants.SERVER_ADDR)

	//server.ListenAndServeTLS(fmt.Sprintf("%s%s", constants.RESOURCES_PATH, constants.CERT_FILE_NAME), fmt.Sprintf("%s%s", constants.RESOURCES_PATH, constants.KEY_FILE_NAME))
	server.ListenAndServe()
}
