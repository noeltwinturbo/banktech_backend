package handles

import (
	"banktech_backend/entities"
	"banktech_backend/models"
	"banktech_backend/utils/constants"
	"banktech_backend/utils/constants/envConfigConstants"
	"banktech_backend/utils/constants/fieldConstants"
	"banktech_backend/utils/constants/messageConstants"
	"banktech_backend/utils/loggerUtils"
	"banktech_backend/utils/restUtils"
	"banktech_backend/utils/tokenUtils"
	"banktech_backend/validatorsEngine"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

//Do /envconfigs GET actions
func GetEnvConfigs(w http.ResponseWriter, r *http.Request) {
	var envConfigsModel models.EnvConfigsModel

	userId, isAdmin := tokenUtils.ValidateToken(w, r)
	if userId != nil {
		if !*isAdmin {
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_ENV_CONFIG_USER_NOT_ADMIN)
		} else {
			envConfigs, err := envConfigsModel.FindAllEnvConfigs()
			if err != nil {
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			} else {
				restUtils.SetOKResponseBody(w, http.StatusOK, envConfigs)
			}
		}
	}
}

//Do /envconfigs POST actions
func PostEnvConfigs(w http.ResponseWriter, r *http.Request) {
	var envConfig entities.EnvConfig
	var envConfigsModel models.EnvConfigsModel
	var envConfigsValidator validatorsEngine.EnvConfigsValidator

	userId, isAdmin := tokenUtils.ValidateToken(w, r)
	if userId != nil {
		if !*isAdmin {
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_ENV_CONFIG_USER_NOT_ADMIN)
		} else {
			err := json.NewDecoder(r.Body).Decode(&envConfig)
			if err != nil {
				loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al deserializar el body de la petición. Error: %s", err)
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			} else {
				if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
					input, _ := json.Marshal(&envConfig)
					loggerUtils.LOGGER_FINE.Printf("Body de entrada: %s", input)
				}
				err = envConfigsValidator.ValidateInsertEnvConfig(&envConfig)
				if err != nil {
					restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, err.Error())
				} else {
					err = envConfigsModel.InsertEnvConfig(&envConfig)
					if err != nil {
						restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
					} else {
						restUtils.SetOKOResponseMessage(w, http.StatusOK, messageConstants.REST_ENV_CONFIG_INSERTED)
					}
				}
			}
		}
	}
}

//Do /envconfigs PUT actions
func PutEnvConfigs(w http.ResponseWriter, r *http.Request) {
	var envConfig entities.EnvConfig
	var envConfigsModel models.EnvConfigsModel
	var validatorEngine validatorsEngine.ValidatorEngine

	userId, isAdmin := tokenUtils.ValidateToken(w, r)
	if userId != nil {
		if !*isAdmin {
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_ENV_CONFIG_USER_NOT_ADMIN)
		} else {
			err := json.NewDecoder(r.Body).Decode(&envConfig)
			if err != nil {
				loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al deserializar el body de la petición. Error: %s", err)
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			} else {
				err = validatorEngine.ValidateStruct(envConfig)
				if err != nil {
					restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, err.Error())
				} else {
					updated, err := envConfigsModel.UpdateEnvConfig(&envConfig)
					if err != nil {
						restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
					} else if updated == 0 {
						restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, fmt.Sprintf(messageConstants.REST_ENV_CONFIG_NOT_FOUND, envConfig.ConfigName))
					} else {
						restUtils.SetOKOResponseMessage(w, http.StatusOK, messageConstants.REST_ENV_CONFIG_UPDATED)
					}
				}
			}
		}
	}
}

//Do /envconfigs DELETE actions
func DeleteEnvConfigs(w http.ResponseWriter, r *http.Request) {
	var envConfigsModel models.EnvConfigsModel

	envConfigName := mux.Vars(r)[fieldConstants.FIELD_ENV_CONFIG_NAME]

	userId, isAdmin := tokenUtils.ValidateToken(w, r)
	if userId != nil {
		if !*isAdmin {
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_ENV_CONFIG_USER_NOT_ADMIN)
		} else {
			deleted, err := envConfigsModel.DeleteEnvConfig(envConfigName)
			if err != nil {
				restUtils.SetOKOResponseMessage(w, http.StatusUnauthorized, messageConstants.REST_GENERIC_ERROR)
			} else if deleted == 0 {
				restUtils.SetOKOResponseMessage(w, http.StatusUnauthorized, fmt.Sprintf(messageConstants.REST_ENV_CONFIG_NOT_FOUND, envConfigName))
			} else {
				restUtils.SetOKOResponseMessage(w, http.StatusOK, messageConstants.REST_ENV_CONFIG_DELETED)
			}
		}
	}
}

//Do /envconfigs/reload POST actions
func PostEnvConfigsReload(w http.ResponseWriter, r *http.Request) {
	var envConfigsModel models.EnvConfigsModel

	_, isAdmin := tokenUtils.ValidateToken(w, r)
	if isAdmin != nil {
		if !*isAdmin {
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_ENV_CONFIG_USER_NOT_ADMIN)
		} else {
			err := envConfigsModel.ReloadCacheEnvConfigs()
			if err != nil {
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			} else {
				restUtils.SetOKOResponseMessage(w, http.StatusOK, messageConstants.REST_ENV_CONFIG_RELOADED)
			}
		}
	}
}

//Do /envconfigs/logs GET actions
func GetEnvConfigsLogs(w http.ResponseWriter, r *http.Request) {
	_, isAdmin := tokenUtils.ValidateToken(w, r)
	if isAdmin != nil {
		if !*isAdmin {
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_ENV_CONFIG_USER_NOT_ADMIN)
		} else {
			file, err := os.Open(constants.LOGS_PATH)
			if err != nil {
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			}
			w.Header().Set("Content-Type", "text/plain")
			io.Copy(w, file)
		}
	}
}
