package loggerUtils

import (
	"banktech_backend/utils/constants"
	"fmt"
	"log"
	"os"
)

var LOGGER_ERROR *log.Logger
var LOGGER_INFO *log.Logger
var LOGGER_FINE *log.Logger

//Inits all server logs
func InitLogs() {
	file, err := os.OpenFile(constants.LOGS_PATH, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println(err)
	}

	LOGGER_ERROR = log.New(file,
		"ERROR: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	LOGGER_INFO = log.New(file,
		"INFO: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	LOGGER_FINE = log.New(file,
		"FINE: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	LOGGER_INFO.Printf("Logs iniciados correctamente")
}
