package validatorsEngine

import (
	"banktech_backend/utils/constants"
	"banktech_backend/utils/constants/envConfigConstants"
	"banktech_backend/utils/constants/fieldConstants"
	"banktech_backend/utils/constants/messageConstants"
	"banktech_backend/utils/loggerUtils"
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/go-playground/locales/es"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator"
	en_translations "github.com/go-playground/validator/translations/en"
)

type ValidatorEngine struct {
}

//Executes the Struct input validation
func (this ValidatorEngine) ValidateStruct(entity interface{}) error {
	validate, translator := getValidatorAndTranslator()

	err := validate.Struct(entity)
	if err != nil {
		errField := err.(validator.ValidationErrors)
		if errField != nil && translator != nil {
			return errors.New(errField[0].Translate(translator))
		} else {
			ref := reflect.TypeOf(entity)
			loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al validar los campos del objeto %s. Error: %s", ref.Name(), err)
			return errors.New(messageConstants.REST_GENERIC_ERROR)
		}
	}

	return nil
}

//Gets the validator with the ES translations
func getValidatorAndTranslator() (*validator.Validate, ut.Translator) {
	validate := validator.New()
	trans := es.New()
	uni := ut.New(trans, trans)

	translator, found := uni.GetTranslator("es")
	if !found {
		loggerUtils.LOGGER_ERROR.Printf("No se ha encontrado el traductor ES")
		return validate, nil
	}

	if err := en_translations.RegisterDefaultTranslations(validate, translator); err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al registar el traductor ES por defecto. Error: %s", err)
		return validate, nil
	}

	_ = validate.RegisterValidation(constants.VAL_TYPE_DNI, func(fl validator.FieldLevel) bool {
		match, _ := regexp.MatchString(*envConfigConstants.REGEX_DNI, fl.Field().String())
		if match {
			match = validateDNI(fl.Field().String())
		}
		return match
	})

	_ = validate.RegisterValidation(constants.VAL_TYPE_PASSWORD, func(fl validator.FieldLevel) bool {
		return validatePassword(fl.Field().String())
	})

	_ = validate.RegisterValidation(constants.VAL_TYPE_PHONE, func(fl validator.FieldLevel) bool {
		match, _ := regexp.MatchString(*envConfigConstants.REGEX_PHONE, fl.Field().String())
		return match
	})

	_ = validate.RegisterValidation(constants.VAL_TYPE_ZIPCODE, func(fl validator.FieldLevel) bool {
		match, _ := regexp.MatchString(*envConfigConstants.REGEX_ZIPCODE, fl.Field().String())
		return match
	})

	_ = validate.RegisterValidation(constants.VAL_TYPE_BIRTHDAY, func(fl validator.FieldLevel) bool {
		return validateBirthday(fl.Field().Interface())
	})

	_ = validate.RegisterValidation(constants.VAL_TYPE_IBAN, func(fl validator.FieldLevel) bool {
		match, _ := regexp.MatchString(*envConfigConstants.REGEX_IBAN, fl.Field().String())
		return match
	})

	_ = validate.RegisterValidation(constants.VAL_TYPE_MOVEMENT_TYPE, func(fl validator.FieldLevel) bool {
		return fl.Field().Int() == constants.MOVEMENT_DEPOSIT || fl.Field().Int() == constants.MOVEMENT_TRANSFER || fl.Field().Int() == constants.MOVEMENT_WITHDRAWALS
	})

	_ = validate.RegisterTranslation(constants.VAL_TYPE_REQUIRED, translator, func(ut ut.Translator) error {
		return ut.Add(constants.VAL_TYPE_REQUIRED, messageConstants.TRANS_REQUIRED, true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(constants.VAL_TYPE_REQUIRED, getFieldTransaltion(fe.Field()))
		return t
	})

	_ = validate.RegisterTranslation(constants.VAL_TYPE_MAX, translator, func(ut ut.Translator) error {
		return ut.Add(constants.VAL_TYPE_MAX, messageConstants.TRANS_MAX, true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(constants.VAL_TYPE_MAX, getFieldTransaltion(fe.Field()), fe.Param())
		return t
	})

	_ = validate.RegisterTranslation(constants.VAL_TYPE_MIN, translator, func(ut ut.Translator) error {
		return ut.Add(constants.VAL_TYPE_MIN, messageConstants.TRANS_MIN, true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(constants.VAL_TYPE_MIN, getFieldTransaltion(fe.Field()), fe.Param())
		return t
	})

	_ = validate.RegisterTranslation(constants.VAL_TYPE_NETFIELD, translator, func(ut ut.Translator) error {
		return ut.Add(constants.VAL_TYPE_NETFIELD, messageConstants.TRANS_NETFIELD, true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(constants.VAL_TYPE_NETFIELD, getFieldTransaltion(fe.Field()), getFieldTransaltion(fe.Param()))
		return t
	})

	_ = validate.RegisterTranslation(constants.VAL_TYPE_EMAIL, translator, func(ut ut.Translator) error {
		return ut.Add(constants.VAL_TYPE_EMAIL, messageConstants.TRANS_EMAIL, true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(constants.VAL_TYPE_EMAIL, fmt.Sprintf("%s", fe.Value()))
		return t
	})

	_ = validate.RegisterTranslation(constants.VAL_TYPE_GTE, translator, func(ut ut.Translator) error {
		return ut.Add(constants.VAL_TYPE_GTE, messageConstants.TRANS_GTE, true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(constants.VAL_TYPE_GTE, getFieldTransaltion(fe.Field()), fe.Param())
		return t
	})

	_ = validate.RegisterTranslation(constants.VAL_TYPE_LTE, translator, func(ut ut.Translator) error {
		return ut.Add(constants.VAL_TYPE_LTE, messageConstants.TRANS_LTE, true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(constants.VAL_TYPE_LTE, getFieldTransaltion(fe.Field()), fe.Param())
		return t
	})

	_ = validate.RegisterTranslation(constants.VAL_TYPE_DNI, translator, func(ut ut.Translator) error {
		return ut.Add(constants.VAL_TYPE_DNI, messageConstants.TRANS_DNI, true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(constants.VAL_TYPE_DNI, fmt.Sprintf("%s", fe.Value()))
		return t
	})

	_ = validate.RegisterTranslation(constants.VAL_TYPE_PHONE, translator, func(ut ut.Translator) error {
		return ut.Add(constants.VAL_TYPE_PHONE, messageConstants.TRANS_PHONE, true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(constants.VAL_TYPE_PHONE, fmt.Sprintf("%s", fe.Value()))
		return t
	})

	_ = validate.RegisterTranslation(constants.VAL_TYPE_ZIPCODE, translator, func(ut ut.Translator) error {
		return ut.Add(constants.VAL_TYPE_ZIPCODE, messageConstants.TRANS_ZIPCODE, true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(constants.VAL_TYPE_ZIPCODE, fmt.Sprintf("%s", fe.Value()))
		return t
	})

	_ = validate.RegisterTranslation(constants.VAL_TYPE_PASSWORD, translator, func(ut ut.Translator) error {
		return ut.Add(constants.VAL_TYPE_PASSWORD, messageConstants.TRANS_PASSWORD, true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(constants.VAL_TYPE_PASSWORD, fmt.Sprintf("%s", fe.Value()))
		return t
	})

	_ = validate.RegisterTranslation(constants.VAL_TYPE_BIRTHDAY, translator, func(ut ut.Translator) error {
		return ut.Add(constants.VAL_TYPE_BIRTHDAY, messageConstants.TRANS_BIRTHDAY, true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(constants.VAL_TYPE_BIRTHDAY, strings.Split(fmt.Sprintf("%s", fe.Value()), " ")[0])
		return t
	})

	_ = validate.RegisterTranslation(constants.VAL_TYPE_IBAN, translator, func(ut ut.Translator) error {
		return ut.Add(constants.VAL_TYPE_IBAN, messageConstants.TRANS_IBAN, true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(constants.VAL_TYPE_IBAN, fmt.Sprintf("%s", fe.Value()))
		return t
	})

	_ = validate.RegisterTranslation(constants.VAL_TYPE_MOVEMENT_TYPE, translator, func(ut ut.Translator) error {
		return ut.Add(constants.VAL_TYPE_MOVEMENT_TYPE, messageConstants.TRANS_MOVEMENT_TYPE, true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(constants.VAL_TYPE_MOVEMENT_TYPE)
		return t
	})

	return validate, translator
}

//Validates the DNI
func validateDNI(dni string) bool {
	letterArray := []string{"T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"}

	dniNumbers, _ := strconv.Atoi(dni[0:8])
	dniLetter := dni[8:9]
	letter := letterArray[dniNumbers%23]

	return dniLetter == letter
}

//Validates the birthday
func validateBirthday(birthdayRaw interface{}) bool {
	birthday := birthdayRaw.(time.Time)

	years := time.Now().Year() - birthday.Year()
	if time.Now().YearDay() < birthday.YearDay() {
		years--
	}

	return years >= 18
}

//Validates the password
func validatePassword(password string) bool {
	return matchString(*envConfigConstants.REGEX_PASSWORD_1, password) && matchString(*envConfigConstants.REGEX_PASSWORD_2, password) && matchString(*envConfigConstants.REGEX_PASSWORD_3, password) && matchString(*envConfigConstants.REGEX_PASSWORD_4, password) && !matchString(*envConfigConstants.REGEX_PASSWORD_5, password) && matchString(*envConfigConstants.REGEX_PASSWORD_6, password)
}

func matchString(regex string, value string) bool {
	match, _ := regexp.MatchString(regex, value)

	return match
}

//Gets the field in ES translation
func getFieldTransaltion(originalField string) string {
	var toret string

	var sb strings.Builder
	sb.WriteString(strings.ToLower(originalField[0:1]))
	sb.WriteString(originalField[1:len(originalField)])

	switch sb.String() {
	case fieldConstants.FIELD_USER_NAME:
		toret = fieldConstants.ES_FIELD_USER_NAME
	case fieldConstants.FIELD_USER_LAST_NAME:
		toret = fieldConstants.ES_FIELD_USER_LAST_NAME
	case fieldConstants.FIELD_USER_BIRTHDAY:
		toret = fieldConstants.ES_FIELD_USER_BIRTHDAY
	case fieldConstants.FIELD_USER_PHONE:
		toret = fieldConstants.ES_FIELD_USER_PHONE
	case fieldConstants.FIELD_USER_ADDRESS:
		toret = fieldConstants.ES_FIELD_USER_ADDRESS
	case fieldConstants.FIELD_USER_DNI:
		toret = fieldConstants.ES_FIELD_USER_DNI
	case fieldConstants.FIELD_USER_EMAIL:
		toret = fieldConstants.ES_FIELD_USER_EMAIL
	case fieldConstants.FIELD_USER_PASSWORD:
		toret = fieldConstants.ES_FIELD_USER_PASSWORD
	case fieldConstants.FIELD_USER_ID:
		toret = fieldConstants.ES_FIELD_USER_USER_ID
	case fieldConstants.FIELD_USER_USER_CONFIG:
		toret = fieldConstants.ES_FIELD_USER_USER_CONFIG
	case fieldConstants.FIELD_USER_IS_ADMIN:
		toret = fieldConstants.ES_FIELD_USER_IS_ADMIN
	case fieldConstants.FIELD_USER_IS_DELETED:
		toret = fieldConstants.ES_FIELD_USER_IS_DELETED
	case fieldConstants.FIELD_ADDRESS_STREET:
		toret = fieldConstants.ES_FIELD_ADDRESS_STREET
	case fieldConstants.FIELD_ADDRESS_CITY:
		toret = fieldConstants.ES_FIELD_ADDRESS_CITY
	case fieldConstants.FIELD_ADDRESS_COUNTRY:
		toret = fieldConstants.ES_FIELD_ADDRESS_COUNTRY
	case fieldConstants.FIELD_ADDRESS_ZIPCODE:
		toret = fieldConstants.ES_FIELD_ADDRESS_ZIPCODE
	case fieldConstants.FIELD_USER_CONFIG_IS_GENERAL_NOTIFY_ENABLED:
		toret = fieldConstants.ES_FIELD_USER_CONFIG_IS_GENERAL_NOTIFY_ENABLED
	case fieldConstants.FIELD_USER_CONFIG_IS_MOVEMENTS_NOTIFY_ENABLED:
		toret = fieldConstants.ES_FIELD_USER_CONFIG_IS_MOVEMENTS_NOTIFY_ENABLED
	case fieldConstants.FIELD_USER_CONFIG_MIN_MOV_AMNT_NOTIFY:
		toret = fieldConstants.ES_FIELD_USER_CONFIG_MIN_MOV_AMNT_NOTIFY
	case fieldConstants.FIELD_ACCOUNT_ID:
		toret = fieldConstants.ES_FIELD_ACCOUNT_ID
	case fieldConstants.FIELD_ACCOUNT_IBAN:
		toret = fieldConstants.ES_FIELD_ACCOUNT_IBAN
	case fieldConstants.FIELD_ACCOUNT_ALIAS:
		toret = fieldConstants.ES_FIELD_ACCOUNT_ALIAS
	case fieldConstants.FIELD_ACCOUNT_MOVEMENTS:
		toret = fieldConstants.ES_FIELD_ACCOUNT_MOVEMENTS
	case fieldConstants.FIELD_ACCOUNT_TOTAL_AMOUNT:
		toret = fieldConstants.ES_FIELD_ACCOUNT_TOTAL_AMOUNT
	case fieldConstants.FIELD_ACCOUNT_OWNER:
		toret = fieldConstants.ES_FIELD_ACCOUNT_OWNER
	case fieldConstants.FIELD_MOVEMENT_ID:
		toret = fieldConstants.ES_FIELD_MOVEMENT_ID
	case fieldConstants.FIELD_MOVEMENT_TYPE:
		toret = fieldConstants.ES_FIELD_MOVEMENT_TYPE
	case fieldConstants.FIELD_MOVEMENT_AMOUNT:
		toret = fieldConstants.ES_FIELD_MOVEMENT_AMOUNT
	case fieldConstants.FIELD_MOVEMENT_SENDER_IBAN:
		toret = fieldConstants.ES_FIELD_MOVEMENT_SENDER_IBAN
	case fieldConstants.FIELD_MOVEMENT_RECIPIENT_IBAN:
		toret = fieldConstants.ES_FIELD_MOVEMENT_RECIPIENT_IBAN
	case fieldConstants.FIELD_MOVEMENT_DATE:
		toret = fieldConstants.ES_FIELD_MOVEMENT_DATE
	case fieldConstants.FIELD_MOVEMENT_CONCEPT:
		toret = fieldConstants.ES_FIELD_MOVEMENT_CONCEPT
	case fieldConstants.FIELD_MOVEMENT_IS_CANCELED:
		toret = fieldConstants.ES_FIELD_MOVEMENT_IS_CANCELED
	case fieldConstants.FIELD_ENV_CONFIG_NAME:
		toret = fieldConstants.ES_FIELD_ENV_CONFIG_NAME
	case fieldConstants.FIELD_ENV_CONFIG_VALUE:
		toret = fieldConstants.ES_FIELD_ENV_CONFIG_VALUE
	default:
		toret = originalField
	}

	return toret
}
