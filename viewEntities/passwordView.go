package viewEntities

type PasswordView struct {
	Password string `json:"password" validate:"required,password"`
}
