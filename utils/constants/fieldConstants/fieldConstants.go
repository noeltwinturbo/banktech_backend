package fieldConstants

//ID field constant
const FIELD_ID = "_id"

//UserId field constant
const FIELD_USER_ID = "userId"

//Entities field names constants
const (
	FIELD_USER_NAME        = "name"
	FIELD_USER_LAST_NAME   = "lastName"
	FIELD_USER_BIRTHDAY    = "birthday"
	FIELD_USER_PHONE       = "phone"
	FIELD_USER_ADDRESS     = "address"
	FIELD_USER_DNI         = "dni"
	FIELD_USER_EMAIL       = "email"
	FIELD_USER_PASSWORD    = "password"
	FIELD_USER_USER_CONFIG = "userConfig"
	FIELD_USER_IS_ADMIN    = "isAdmin"
	FIELD_USER_IS_DELETED  = "isDeleted"

	FIELD_ADDRESS_STREET  = "street"
	FIELD_ADDRESS_CITY    = "city"
	FIELD_ADDRESS_COUNTRY = "country"
	FIELD_ADDRESS_ZIPCODE = "zipcode"

	FIELD_USER_CONFIG_IS_GENERAL_NOTIFY_ENABLED   = "isGeneralNotifyEnable"
	FIELD_USER_CONFIG_IS_MOVEMENTS_NOTIFY_ENABLED = "isMovementsNotifyEnable"
	FIELD_USER_CONFIG_MIN_MOV_AMNT_NOTIFY         = "minMovAmntNotify"

	FIELD_ACCOUNT_ID           = "accountId"
	FIELD_ACCOUNT_IBAN         = "iban"
	FIELD_ACCOUNT_ALIAS        = "alias"
	FIELD_ACCOUNT_MOVEMENTS    = "movements"
	FIELD_ACCOUNT_TOTAL_AMOUNT = "totalAmount"
	FIELD_ACCOUNT_OWNER        = "owner"

	FIELD_MOVEMENT_ID             = "movementId"
	FIELD_MOVEMENT_TYPE           = "type"
	FIELD_MOVEMENT_AMOUNT         = "amount"
	FIELD_MOVEMENT_SENDER_IBAN    = "senderIban"
	FIELD_MOVEMENT_RECIPIENT_IBAN = "recipientIban"
	FIELD_MOVEMENT_DATE           = "movementDate"
	FIELD_MOVEMENT_CONCEPT        = "concept"
	FIELD_MOVEMENT_IS_CANCELED    = "isCanceled"

	FIELD_ENV_CONFIG_NAME  = "configName"
	FIELD_ENV_CONFIG_VALUE = "configValue"
)

//UserId ES field constant
const ES_FIELD_USER_USER_ID = "identificador del usuario"

//Entities ES field names constants
const (
	ES_FIELD_USER_NAME        = "nombre"
	ES_FIELD_USER_LAST_NAME   = "apellidos"
	ES_FIELD_USER_BIRTHDAY    = "fecha de nacimiento"
	ES_FIELD_USER_PHONE       = "teléfono"
	ES_FIELD_USER_ADDRESS     = "dirección"
	ES_FIELD_USER_DNI         = "DNI"
	ES_FIELD_USER_EMAIL       = "email"
	ES_FIELD_USER_PASSWORD    = "contraseña"
	ES_FIELD_USER_USER_CONFIG = "configuración de usuario"
	ES_FIELD_USER_IS_ADMIN    = "es administrador"
	ES_FIELD_USER_IS_DELETED  = "está eliminado"

	ES_FIELD_ADDRESS_STREET  = "calle"
	ES_FIELD_ADDRESS_CITY    = "ciudad"
	ES_FIELD_ADDRESS_COUNTRY = "país"
	ES_FIELD_ADDRESS_ZIPCODE = "código postal"

	ES_FIELD_USER_CONFIG_IS_GENERAL_NOTIFY_ENABLED   = "están las notificaciones generales activadas"
	ES_FIELD_USER_CONFIG_IS_MOVEMENTS_NOTIFY_ENABLED = "están las notificaciones de movimientos activadas"
	ES_FIELD_USER_CONFIG_MIN_MOV_AMNT_NOTIFY         = "movimiento mínimo"

	ES_FIELD_ACCOUNT_ID           = "identificador de cuenta"
	ES_FIELD_ACCOUNT_IBAN         = "IBAN"
	ES_FIELD_ACCOUNT_ALIAS        = "alias"
	ES_FIELD_ACCOUNT_MOVEMENTS    = "movimientos"
	ES_FIELD_ACCOUNT_TOTAL_AMOUNT = "saldo"
	ES_FIELD_ACCOUNT_OWNER        = "propietario"

	ES_FIELD_MOVEMENT_ID             = "identificador de movimiento"
	ES_FIELD_MOVEMENT_TYPE           = "tipo"
	ES_FIELD_MOVEMENT_AMOUNT         = "total"
	ES_FIELD_MOVEMENT_SENDER_IBAN    = "IBAN emisor"
	ES_FIELD_MOVEMENT_RECIPIENT_IBAN = "IBAN receptor"
	ES_FIELD_MOVEMENT_DATE           = "fecha movimiento"
	ES_FIELD_MOVEMENT_CONCEPT        = "concepto"
	ES_FIELD_MOVEMENT_IS_CANCELED    = "es cancelado"

	ES_FIELD_ENV_CONFIG_NAME  = "nombre de la configuración"
	ES_FIELD_ENV_CONFIG_VALUE = "valor de la configuración"
)
