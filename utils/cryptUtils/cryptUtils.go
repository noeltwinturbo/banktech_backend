package cryptUtils

import (
	"banktech_backend/utils/constants"
	"banktech_backend/utils/loggerUtils"
	"crypto/aes"
	"crypto/cipher"
	crand "crypto/rand"
	"encoding/base64"
	"io"
	mrand "math/rand"
	"strings"
	"time"
)

//Encrypts an input string
func Encrypt(plainText string) (*string, error) {
	block, err := aes.NewCipher(constants.KEY)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al instanciar un nuevo cifrador. Error: %s", err)
		return nil, err
	}

	plaintTextArr := []byte(plainText)

	b := base64.StdEncoding.EncodeToString([]byte(plaintTextArr))
	ciphertext := make([]byte, aes.BlockSize+len(b))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(crand.Reader, iv); err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al encriptar el valor %s. Error: %s", plainText, err)
		return nil, err
	}

	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(ciphertext[aes.BlockSize:], []byte(b))

	ciphertextStr := string(ciphertext)

	return &ciphertextStr, nil
}

//Decrypts an encrypted input string
func Decrypt(encryptedText string) (*string, error) {
	block, err := aes.NewCipher(constants.KEY)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al instanciar un nuevo cifrador. Error: %s", err)
		return nil, err
	}

	encryptedArr := []byte(encryptedText)

	iv := encryptedArr[:aes.BlockSize]
	encryptedArr = encryptedArr[aes.BlockSize:]
	cfb := cipher.NewCFBDecrypter(block, iv)
	cfb.XORKeyStream(encryptedArr, encryptedArr)
	data, err := base64.StdEncoding.DecodeString(string(encryptedArr))
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al desencriptar el valor %s. Error: %s", encryptedText, err)
		return nil, err
	}

	dataStr := string(data)

	return &dataStr, nil
}

//Generates a random user password
func GenerateRandomPassword() string {
	mrand.Seed(time.Now().UnixNano())

	chars := []rune(constants.RANDOM_PASSWORD_CHARS)
	var sb strings.Builder
	for i := 0; i < constants.RANDOM_PASSWORD_LENGTH; i++ {
		sb.WriteRune(chars[mrand.Intn(len(chars))])
	}

	return sb.String()
}
