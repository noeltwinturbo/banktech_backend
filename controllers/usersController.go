package controllers

import (
	"banktech_backend/handles"
	"banktech_backend/utils/loggerUtils"
	"net/http"

	"github.com/gorilla/mux"
)

//Inits Users controllers
func loadUsersControllers(r *mux.Router) {
	loggerUtils.LOGGER_INFO.Printf("Iniciando Users Controllers")

	r.HandleFunc("/users", handles.GetUsers).Methods(http.MethodGet)
	r.HandleFunc("/users", handles.PostUsers).Methods(http.MethodPost)
	r.HandleFunc("/users", handles.DeleteUsers).Methods(http.MethodDelete)
	r.HandleFunc("/users/login", handles.PostUsersLogin).Methods(http.MethodPost)
	r.HandleFunc("/users/logout", handles.PostUsersLogout).Methods(http.MethodPost)
	r.HandleFunc("/users/data", handles.PutUsersData).Methods(http.MethodPut)
	r.HandleFunc("/users/email", handles.PutUsersEmail).Methods(http.MethodPut)
	r.HandleFunc("/users/dni", handles.PutUsersDni).Methods(http.MethodPut)
	r.HandleFunc("/users/password", handles.PutUsersPassword).Methods(http.MethodPut)
	r.HandleFunc("/users/config", handles.PutUsersConfig).Methods(http.MethodPut)
	r.HandleFunc("/users/resetPassword", handles.PostUsersResetPassword).Methods(http.MethodPost)
}
