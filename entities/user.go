package entities

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	UserId     primitive.ObjectID `json:"userId,omitempty" bson:"_id"`
	Name       string             `json:"name" bson:"name" validate:"required,min=3,max=25"`
	LastName   string             `json:"lastName" bson:"lastName" validate:"required,min=3,max=50"`
	Birthday   time.Time          `json:"birthday" bson:"birthday" validate:"required,birthday"`
	Phone      string             `json:"phone" bson:"phone" validate:"required,phone"`
	Address    Address            `json:"address" bson:"address" validate:"required"`
	Dni        string             `json:"dni" bson:"dni" validate:"required,dni"`
	Email      string             `json:"email" bson:"email" validate:"required,email"`
	Password   string             `json:"password" bson:"password" validate:"required,password"`
	UserConfig UserConfig         `json:"userConfig" bson:"userConfig" validate:"required"`
	IsAdmin    bool               `json:"isAdmin" bson:"isAdmin"`
	IsDeleted  bool               `json:"isDeleted" bson:"isDeleted"`
}
