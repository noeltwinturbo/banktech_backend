package envConfigConstants

//Regex vars
var (
	REGEX_PASSWORD_1      *string
	REGEX_PASSWORD_2      *string
	REGEX_PASSWORD_3      *string
	REGEX_PASSWORD_4      *string
	REGEX_PASSWORD_5      *string
	REGEX_PASSWORD_6      *string
	REGEX_PHONE           *string
	REGEX_ZIPCODE         *string
	REGEX_DNI             *string
	REGEX_IBAN            *string
	IS_LOGGER_FINE_ENABLE *bool
)

//Regex default constants
const (
	REGEX_DEFAULT_PASSWORD_1      = "[a-z]+"
	REGEX_DEFAULT_PASSWORD_2      = "[A-Z]+"
	REGEX_DEFAULT_PASSWORD_3      = "[0-9]+"
	REGEX_DEFAULT_PASSWORD_4      = "[,;.:-_!@#$%^&*]+"
	REGEX_DEFAULT_PASSWORD_5      = "\\s+"
	REGEX_DEFAULT_PASSWORD_6      = "^.{8,16}$"
	REGEX_DEFAULT_PHONE           = "^\\d{9}$"
	REGEX_DEFAULT_ZIPCODE         = "^\\d{5}$"
	REGEX_DEFAULT_DNI             = "^\\d{8}[A-Z]$"
	REGEX_DEFAULT_IBAN            = "^[A-Z]{2}\\d{22}$"
	IS_DEFAULT_LOGGER_FINE_ENABLE = false
)

//Regex name constants
const (
	REGEX_NAME_PASSWORD_1      = "REGEX_PASSWORD_1"
	REGEX_NAME_PASSWORD_2      = "REGEX_PASSWORD_2"
	REGEX_NAME_PASSWORD_3      = "REGEX_PASSWORD_3"
	REGEX_NAME_PASSWORD_4      = "REGEX_PASSWORD_4"
	REGEX_NAME_PASSWORD_5      = "REGEX_PASSWORD_5"
	REGEX_NAME_PASSWORD_6      = "REGEX_PASSWORD_6"
	REGEX_NAME_PHONE           = "REGEX_PHONE"
	REGEX_NAME_ZIPCODE         = "REGEX_ZIPCODE"
	REGEX_NAME_DNI             = "REGEX_DNI"
	REGEX_NAME_IBAN            = "REGEX_IBAN"
	IS_NAME_LOGGER_FINE_ENABLE = "IS_LOGGER_FINE_ENABLE"
)
