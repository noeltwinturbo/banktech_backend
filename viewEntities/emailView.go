package viewEntities

type EmailView struct {
	Email string `json:"email" validate:"required,email"`
}
