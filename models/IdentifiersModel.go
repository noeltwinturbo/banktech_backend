package models

import (
	"banktech_backend/entities"
	"banktech_backend/utils/constants"
	"banktech_backend/utils/constants/fieldConstants"
	"banktech_backend/utils/loggerUtils"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IdentifiersModel struct {
}

//Gets a new movement identifier
func (this IdentifiersModel) GetNewMovementIdentifier() (*int64, error) {
	var identifier entities.Identifier

	result, err := findEntityByFields(constants.COLL_IDENTIFIERS, bson.M{})
	if err != nil || result == nil {
		return nil, err
	}

	err = result.Decode(&identifier)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al decodificar el documento Identifier en la BBDD. Error: %s", err)
		return nil, err
	}

	filter := bson.M{fieldConstants.FIELD_ID: identifier.IdentifierId}

	update := bson.D{
		{"$set", bson.D{
			{fieldConstants.FIELD_MOVEMENT_ID, identifier.MovementId + 1},
		}},
	}
	updated, err := updateEntityByFields(constants.COLL_IDENTIFIERS, filter, update)
	if err != nil || updated == 0 {
		return nil, err
	}

	return &identifier.MovementId, nil
}

//Creates zero identifiers if it does not exist
func createZeroIdentifiers() {
	var identifier entities.Identifier

	loggerUtils.LOGGER_INFO.Printf("Creando los identifiers con valor cero")

	identifier.IdentifierId = primitive.NewObjectID()
	identifier.MovementId = 1

	_, err := insertEntity(constants.COLL_IDENTIFIERS, identifier)
	if err != nil {
		panic(err)
	}
}
