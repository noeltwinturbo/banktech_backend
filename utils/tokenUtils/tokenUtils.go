package tokenUtils

import (
	"banktech_backend/entities"
	"banktech_backend/utils/constants"
	"banktech_backend/utils/constants/envConfigConstants"
	"banktech_backend/utils/constants/messageConstants"
	"banktech_backend/utils/loggerUtils"
	"banktech_backend/utils/restUtils"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//Generates an user token with the user info
func GenerateToken(user *entities.User) (*http.Cookie, error) {
	var toret http.Cookie

	expirationTime := time.Now().Add(constants.TOKEN_EXPIRATION * time.Minute)

	claims := &entities.Claims{
		UserId:  user.UserId,
		IsAdmin: user.IsAdmin,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(constants.KEY)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al intentar firmar el token. Error: %s", err)
		return nil, err
	}

	toret = http.Cookie{
		Name:    constants.TOKEN_NAME,
		Value:   tokenString,
		Expires: expirationTime,
		Path:    "/",
	}

	return &toret, nil
}

//Makes that the current token expire
func ExpireCurrentToken() *http.Cookie {
	expirationTime := time.Now().Add(-constants.TOKEN_EXPIRATION * time.Minute)
	return &http.Cookie{
		Name:    constants.TOKEN_NAME,
		Value:   "",
		Expires: expirationTime,
	}
}

//Validates an input user token
func ValidateToken(w http.ResponseWriter, r *http.Request) (*primitive.ObjectID, *bool) {
	claims := &entities.Claims{}

	c, err := r.Cookie(constants.TOKEN_NAME)
	if err != nil {
		if err == http.ErrNoCookie {
			if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
				loggerUtils.LOGGER_FINE.Printf("No se ha encontrado la cookie con el token. Error: %s", err)
			}
			restUtils.SetOKOResponseMessage(w, http.StatusUnauthorized, messageConstants.REST_USER_NOT_AUTHENTICATED)
			return nil, nil
		}
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al intentar recuperar el token de las cookies. Error: %s", err)
		restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
		return nil, nil
	} else {
		tknStr := c.Value
		tkn, err := jwt.ParseWithClaims(tknStr, claims, func(token *jwt.Token) (interface{}, error) {
			return constants.KEY, nil
		})
		if err != nil {
			if err == jwt.ErrSignatureInvalid {
				if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
					loggerUtils.LOGGER_FINE.Printf("La firma del token no es válida. Error: %s", err)
				}
				restUtils.SetOKOResponseMessage(w, http.StatusUnauthorized, messageConstants.REST_USER_NOT_AUTHENTICATED)
				return nil, nil
			} else {
				verr := err.(*jwt.ValidationError)
				if verr.Errors&(jwt.ValidationErrorExpired) != 0 {
					restUtils.SetOKOResponseMessage(w, http.StatusUnauthorized, messageConstants.REST_USER_TOKEN_EXPRIED)
					return nil, nil
				}
			}
			loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al intentar parsear el token. Error: %s", err)
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			return nil, nil
		} else if !tkn.Valid {
			if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
				loggerUtils.LOGGER_FINE.Printf("El token no es válido")
			}
			restUtils.SetOKOResponseMessage(w, http.StatusUnauthorized, messageConstants.REST_USER_NOT_AUTHENTICATED)
			return nil, nil
		}
	}

	return &claims.UserId, &claims.IsAdmin
}
