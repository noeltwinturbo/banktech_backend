package viewEntities

type AccountAliasView struct {
	Alias string `json:"alias" validate:"required,min=3,max=50"`
}
