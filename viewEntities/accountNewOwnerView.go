package viewEntities

type AccountNewOwnerView struct {
	Dni string `json:"dni" validate:"required,dni"`
}
