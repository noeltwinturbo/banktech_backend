package models

import (
	"banktech_backend/entities"
	"banktech_backend/utils/constants"
	"banktech_backend/utils/constants/envConfigConstants"
	"banktech_backend/utils/constants/fieldConstants"
	"banktech_backend/utils/loggerUtils"
	"context"
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
)

type EnvConfigsModel struct {
}

//Finds all envConfigs
func (this EnvConfigsModel) FindAllEnvConfigs() (*[]entities.EnvConfig, error) {
	envConfigs := []entities.EnvConfig{}
	var envConfig entities.EnvConfig

	result, err := findEntitiesByFields(constants.COLL_ENV_CONFIGS, bson.M{})
	if err != nil {
		return nil, err
	}

	for result.Next(context.TODO()) {
		err := result.Decode(&envConfig)
		if err != nil {
			loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al decodificar uno de los resultados de obtener las variables de entorno en la BBDD. Error: %s", err)
		} else {
			envConfigs = append(envConfigs, envConfig)
		}
	}

	return &envConfigs, nil
}

//Insert an envConfig
func (this EnvConfigsModel) InsertEnvConfig(envConfig *entities.EnvConfig) error {
	_, err := insertEntity(constants.COLL_ENV_CONFIGS, envConfig)

	return err
}

//Updates an envConfig by name
func (this EnvConfigsModel) UpdateEnvConfig(envConfig *entities.EnvConfig) (int64, error) {
	filter := bson.M{fieldConstants.FIELD_ENV_CONFIG_NAME: envConfig.ConfigName}

	update := bson.D{
		{"$set", bson.D{
			{fieldConstants.FIELD_ENV_CONFIG_VALUE, envConfig.ConfigValue},
		}},
	}

	updated, err := updateEntityByFields(constants.COLL_ENV_CONFIGS, filter, update)
	return updated, err
}

//Deletes an envConfig
func (this EnvConfigsModel) DeleteEnvConfig(envConfigName string) (int64, error) {
	deleted, err := deleteEntityByFields(constants.COLL_ENV_CONFIGS, bson.M{fieldConstants.FIELD_ENV_CONFIG_NAME: envConfigName})

	return deleted, err
}

//Counts the envConfigs
func (this EnvConfigsModel) CountEnvConfigsByName(envConfigName string) (int64, error) {
	result, err := countEntitiesByFields(constants.COLL_ENV_CONFIGS, bson.M{fieldConstants.FIELD_ENV_CONFIG_NAME: envConfigName})

	return result, err
}

//Reloads the envConfigs into cache
func (this EnvConfigsModel) ReloadCacheEnvConfigs() error {
	loggerUtils.LOGGER_INFO.Printf("Refrescando las configuraciones de entorno en la caché")

	envConfigs, err := this.FindAllEnvConfigs()
	if err != nil {
		return err
	}

	resetEnvConfigs()

	for _, envConfig := range *envConfigs {
		switch envConfig.ConfigName {
		case envConfigConstants.REGEX_NAME_PASSWORD_1:
			envConfigConstants.REGEX_PASSWORD_1 = new(string)
			envConfigConstants.REGEX_PASSWORD_1 = &envConfig.ConfigValue
		case envConfigConstants.REGEX_NAME_PASSWORD_2:
			envConfigConstants.REGEX_PASSWORD_2 = new(string)
			envConfigConstants.REGEX_PASSWORD_2 = &envConfig.ConfigValue
		case envConfigConstants.REGEX_NAME_PASSWORD_3:
			envConfigConstants.REGEX_PASSWORD_3 = new(string)
			envConfigConstants.REGEX_PASSWORD_3 = &envConfig.ConfigValue
		case envConfigConstants.REGEX_NAME_PASSWORD_4:
			envConfigConstants.REGEX_PASSWORD_4 = new(string)
			envConfigConstants.REGEX_PASSWORD_4 = &envConfig.ConfigValue
		case envConfigConstants.REGEX_NAME_PASSWORD_5:
			envConfigConstants.REGEX_PASSWORD_5 = new(string)
			envConfigConstants.REGEX_PASSWORD_5 = &envConfig.ConfigValue
		case envConfigConstants.REGEX_NAME_PASSWORD_6:
			envConfigConstants.REGEX_PASSWORD_6 = new(string)
			envConfigConstants.REGEX_PASSWORD_6 = &envConfig.ConfigValue
		case envConfigConstants.REGEX_NAME_PHONE:
			envConfigConstants.REGEX_PHONE = new(string)
			envConfigConstants.REGEX_PHONE = &envConfig.ConfigValue
		case envConfigConstants.REGEX_NAME_ZIPCODE:
			envConfigConstants.REGEX_ZIPCODE = new(string)
			envConfigConstants.REGEX_ZIPCODE = &envConfig.ConfigValue
		case envConfigConstants.REGEX_NAME_DNI:
			envConfigConstants.REGEX_DNI = new(string)
			envConfigConstants.REGEX_DNI = &envConfig.ConfigValue
		case envConfigConstants.REGEX_NAME_IBAN:
			envConfigConstants.REGEX_IBAN = new(string)
			envConfigConstants.REGEX_IBAN = &envConfig.ConfigValue
		case envConfigConstants.IS_NAME_LOGGER_FINE_ENABLE:
			envConfigConstants.IS_LOGGER_FINE_ENABLE = new(bool)
			is, _ := strconv.ParseBool(envConfig.ConfigValue)
			envConfigConstants.IS_LOGGER_FINE_ENABLE = &is
		}
	}

	checkNilEnvConfigs()

	return nil
}

//Resets the envConfigs
func resetEnvConfigs() {
	envConfigConstants.REGEX_PASSWORD_1 = nil
	envConfigConstants.REGEX_PASSWORD_2 = nil
	envConfigConstants.REGEX_PASSWORD_3 = nil
	envConfigConstants.REGEX_PASSWORD_4 = nil
	envConfigConstants.REGEX_PASSWORD_5 = nil
	envConfigConstants.REGEX_PASSWORD_6 = nil
	envConfigConstants.REGEX_PHONE = nil
	envConfigConstants.REGEX_ZIPCODE = nil
	envConfigConstants.REGEX_DNI = nil
	envConfigConstants.REGEX_IBAN = nil
	envConfigConstants.IS_LOGGER_FINE_ENABLE = nil
}

//Checks nil envConfigs to assings default values
func checkNilEnvConfigs() {
	if envConfigConstants.REGEX_PASSWORD_1 == nil {
		envConfigConstants.REGEX_PASSWORD_1 = new(string)
		*envConfigConstants.REGEX_PASSWORD_1 = envConfigConstants.REGEX_DEFAULT_PASSWORD_1
	}
	if envConfigConstants.REGEX_PASSWORD_2 == nil {
		envConfigConstants.REGEX_PASSWORD_2 = new(string)
		*envConfigConstants.REGEX_PASSWORD_2 = envConfigConstants.REGEX_DEFAULT_PASSWORD_2
	}
	if envConfigConstants.REGEX_PASSWORD_3 == nil {
		envConfigConstants.REGEX_PASSWORD_3 = new(string)
		*envConfigConstants.REGEX_PASSWORD_3 = envConfigConstants.REGEX_DEFAULT_PASSWORD_3
	}
	if envConfigConstants.REGEX_PASSWORD_4 == nil {
		envConfigConstants.REGEX_PASSWORD_4 = new(string)
		*envConfigConstants.REGEX_PASSWORD_4 = envConfigConstants.REGEX_DEFAULT_PASSWORD_4
	}
	if envConfigConstants.REGEX_PASSWORD_5 == nil {
		envConfigConstants.REGEX_PASSWORD_5 = new(string)
		*envConfigConstants.REGEX_PASSWORD_5 = envConfigConstants.REGEX_DEFAULT_PASSWORD_5
	}
	if envConfigConstants.REGEX_PASSWORD_6 == nil {
		envConfigConstants.REGEX_PASSWORD_6 = new(string)
		*envConfigConstants.REGEX_PASSWORD_6 = envConfigConstants.REGEX_DEFAULT_PASSWORD_6
	}
	if envConfigConstants.REGEX_PHONE == nil {
		envConfigConstants.REGEX_PHONE = new(string)
		*envConfigConstants.REGEX_PHONE = envConfigConstants.REGEX_DEFAULT_PHONE
	}
	if envConfigConstants.REGEX_ZIPCODE == nil {
		envConfigConstants.REGEX_ZIPCODE = new(string)
		*envConfigConstants.REGEX_ZIPCODE = envConfigConstants.REGEX_DEFAULT_ZIPCODE
	}
	if envConfigConstants.REGEX_DNI == nil {
		envConfigConstants.REGEX_DNI = new(string)
		*envConfigConstants.REGEX_DNI = envConfigConstants.REGEX_DEFAULT_DNI
	}
	if envConfigConstants.REGEX_IBAN == nil {
		envConfigConstants.REGEX_IBAN = new(string)
		*envConfigConstants.REGEX_IBAN = envConfigConstants.REGEX_DEFAULT_IBAN
	}
	if envConfigConstants.IS_LOGGER_FINE_ENABLE == nil {
		envConfigConstants.IS_LOGGER_FINE_ENABLE = new(bool)
		*envConfigConstants.IS_LOGGER_FINE_ENABLE = envConfigConstants.IS_DEFAULT_LOGGER_FINE_ENABLE
	}
}
