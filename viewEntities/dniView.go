package viewEntities

type DniView struct {
	Dni string `json:"dni" validate:"required,dni"`
}
