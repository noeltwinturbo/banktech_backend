package entities

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Account struct {
	AccountId   primitive.ObjectID `json:"accountId,omitempty" bson:"_id"`
	Iban        string             `json:"iban" bson:"iban" validate:"required,iban"`
	Alias       string             `json:"alias" bson:"alias" validate:"required,min=3,max=50"`
	Owner       string             `json:"owner" bson:"owner" validate:"required,dni"`
	Movements   []Movement         `json:"movements" bson:"movements"`
	TotalAmount float32            `json:"totalAmount" bson:"totalAmount,omitempty"`
	Date        time.Time          `json:"date" bson:"date"`
	UserId      primitive.ObjectID `json:"userId" bson:"userId"`
}
