package validatorsEngine

import (
	"banktech_backend/entities"
	"banktech_backend/models"
	"banktech_backend/utils/constants/messageConstants"
	"banktech_backend/utils/cryptUtils"
	"banktech_backend/viewEntities"
	"errors"
	"fmt"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UsersValidator struct {
}

//Executes the Users input validation before insert into the DDBB
func (this UsersValidator) ValidateInsertUser(user *entities.User) error {
	var validatorEngine ValidatorEngine

	err := validatorEngine.ValidateStruct(user)
	if err != nil {
		return err
	}

	err = validateIfDniExists(user.Dni)
	if err != nil {
		return err
	}

	err = validateIfEmailExists(user.Email)
	if err != nil {
		return err
	}

	password, err := cryptUtils.Encrypt(user.Password)
	if err != nil {
		return errors.New(messageConstants.REST_GENERIC_ERROR)
	}
	user.Password = *password

	user.UserId = primitive.NewObjectID()
	user.IsAdmin = false
	user.IsDeleted = false

	return nil
}

//Executes the Users input validation before update into the DDBB
func (this UsersValidator) ValidateUpdateUserDni(dniView *viewEntities.DniView) error {
	var validatorEngine ValidatorEngine

	err := validatorEngine.ValidateStruct(dniView)
	if err != nil {
		return err
	}

	err = validateIfDniExists(dniView.Dni)
	if err != nil {
		return err
	}

	return nil
}

//Executes the Users input validation before update into the DDBB
func (this UsersValidator) ValidateUpdateUserEmail(emailView *viewEntities.EmailView) error {
	var validatorEngine ValidatorEngine

	err := validatorEngine.ValidateStruct(emailView)
	if err != nil {
		return err
	}

	err = validateIfEmailExists(emailView.Email)
	if err != nil {
		return err
	}

	return nil
}

//Checks if the User DNI exists
func validateIfDniExists(dni string) error {
	var usersModel models.UsersModel

	result, err := usersModel.CountUsersByDNI(dni)
	if err != nil {
		return errors.New(messageConstants.REST_GENERIC_ERROR)
	} else if result > 0 {
		return errors.New(fmt.Sprintf(messageConstants.REST_USER_DUPLICATE_DNI, dni))
	}

	return nil
}

//Checks if the User Email exists
func validateIfEmailExists(email string) error {
	var usersModel models.UsersModel

	result, err := usersModel.CountUsersByEmail(email)
	if err != nil {
		return errors.New(messageConstants.REST_GENERIC_ERROR)
	} else if result > 0 {
		return errors.New(fmt.Sprintf(messageConstants.REST_USER_DUPLICATE_EMAIL, email))
	}

	return nil
}
