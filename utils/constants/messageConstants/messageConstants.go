package messageConstants

//Response messages constants
const (
	REST_GENERIC_ERROR = "Parece que algo no ha salido bien..."

	REST_LOGIN_WRONG_PASSWORD   = "La contraseña es incorrecta"
	REST_LOGIN_USER_NOT_FOUND   = "No existe ningún usuario con el correo %s"
	REST_USER_NOT_AUTHENTICATED = "No te has autenticado todavía!"
	REST_USER_TOKEN_EXPRIED     = "La sesión ha caducado"
	REST_USER_DUPLICATE_EMAIL   = "El email %s ya está registrado"
	REST_USER_DUPLICATE_DNI     = "El DNI %s ya está registrado"
	REST_USER_DATA_UPDATED      = "Los datos han sido actualizados"
	REST_USER_DNI_UPDATED       = "El DNI ha sido actualizado"
	REST_USER_PASSWORD_UPDATED  = "La contraseña ha sido actualizada"
	REST_USER_EMAIL_UPDATED     = "El email ha sido actualizado"
	REST_USER_CONFIG_UPDATED    = "Las configuraciones han sido actualizadas"
	REST_USER_PASSWORD_RESET    = "Te hemos enviado un mail con la nueva contraseña"
	REST_USER_ADMIN_DELETE      = "El usuario administrador no puede ser eliminado"
	REST_USER_ADMIN_UPDATE      = "El usuario administrador no puede actualizar sus datos"
	REST_USER_DELETED           = "Tu cuenta ha sido eliminada"

	REST_ENV_CONFIG_USER_NOT_ADMIN = "Solo el usuario administrador puede acceder y modificar las configuraciones de entorno"
	REST_ENV_CONFIG_NOT_FOUND      = "No existe ninguna configuración de entorno con el nombre \"%s\""
	REST_ENV_CONFIG_DUPLICATE      = "La configuración de entorno \"%s\" ya está registrada"
	REST_ENV_CONFIG_INSERTED       = "La configuración de entorno se ha añadido correctamente"
	REST_ENV_CONFIG_UPDATED        = "Configuración de entorno actualizada"
	REST_ENV_CONFIG_DELETED        = "Configuración de entorno eliminada"
	REST_ENV_CONFIG_RELOADED       = "Configuraciones de entorno cacheadas"

	REST_ACCOUNT_NOT_FOUND      = "No existe ninguna cuenta bancaria con el IBAN %s"
	REST_ACCOUNT_DUPLICATE_IBAN = "El IBAN %s ya está en uso por otro usuario"
	REST_ACCOUNT_INSERTED       = "La cuenta bancaria ha sido añadida correctamente"
	REST_ACCOUNT_ALIAS_UPDATED  = "El alias de la cuenta bancaria ha sido actualizado"
	REST_ACCOUNT_DELETED        = "La cuenta bancaria ha sido eliminada"
	REST_ACCOUNT_OWNER_CHANGED  = "La cuenta bancaria ha sido cambiada de titual correctamente"

	REST_MOVEMENT_RECIPIENT_IBAN_NOT_FOUND = "No existe la cuenta con IBAN %s a quien hacer la transferencia"
	REST_MOVEMENT_NOT_ENOUGH_AMOUNT        = "No tienes suficientes fondos para realizar la transferencia"
	REST_MOVEMENT_TIME_EXPIRED             = "No se puede cancelar el movimiento porque han pasado más de 24 horas"
	REST_MOVEMENT_NOT_VALID                = "No se puede cancelar un movimiento de tipo ingreso o retirada en efectivo"
	REST_MOVEMENT_CANCELED                 = "El movimiento ha sido cancelado"
	REST_MOVEMENT_DONE                     = "El movimiento ha sido realizado"
)

//Validator ES messages constants
const (
	TRANS_REQUIRED      = "\"{0}\" es un campo requerido"
	TRANS_MIN           = "{0} no puede tener menos de {1} caracteres"
	TRANS_MAX           = "{0} no puede tener más de {1} caracteres"
	TRANS_NETFIELD      = "{0} no puede ser igual que {1}"
	TRANS_EMAIL         = "{0} no es un email válido"
	TRANS_GTE           = "{0} no puede ser mayor o igual que {1}"
	TRANS_LTE           = "{0} no puede ser menor o igual que {1}"
	TRANS_DNI           = "{0} no es un DNI válido"
	TRANS_PASSWORD      = "{0} no es una contraseña fuerte"
	TRANS_ZIPCODE       = "{0} no es un códidgo postal válido"
	TRANS_PHONE         = "{0} no es un número de télefono válido"
	TRANS_BIRTHDAY      = "{0} no es una fecha de nacimiento válida, tienes que ser mayor de 18 años"
	TRANS_IBAN          = "{0} no es un IBAN válido"
	TRANS_MOVEMENT_TYPE = "El tipo de transferencia solicitada no es válida"
)
