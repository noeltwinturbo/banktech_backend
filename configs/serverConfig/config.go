package serverConfig

import (
	"banktech_backend/utils/constants"
	"banktech_backend/utils/loggerUtils"
	"crypto/tls"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

//Returns the server config
func GetServerConfig(r *mux.Router) *http.Server {
	loggerUtils.LOGGER_INFO.Printf("Obteniendo configuración del servidor")

	tlsConfig := &tls.Config{
		MinVersion: tls.VersionTLS12,
	}

	return &http.Server{
		Addr:         constants.SERVER_ADDR,
		Handler:      r,
		ReadTimeout:  constants.SERVER_READ_TIMEOUT * time.Second,
		WriteTimeout: constants.SERVER_WRITE_TIMEOUT * time.Second,
		TLSConfig:    tlsConfig,
	}
}
