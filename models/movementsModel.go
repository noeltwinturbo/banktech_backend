package models

import (
	"banktech_backend/entities"
	"banktech_backend/utils/constants"
	"banktech_backend/utils/constants/fieldConstants"
	"banktech_backend/utils/constants/messageConstants"
	"banktech_backend/utils/loggerUtils"
	"context"
	"errors"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type MovementsModel struct {
}

//Finds movements by userId and accountId
func (this MovementsModel) FindMovements(userId primitive.ObjectID, accountId primitive.ObjectID) (*[]entities.Movement, error) {
	movements := []entities.Movement{}
	var account entities.Account
	var accountsModel AccountsModel

	myAccount, err := accountsModel.FindAccountByUserIdAndAccountId(userId, accountId, false)
	if err != nil {
		return nil, err
	} else if myAccount == nil {
		return nil, errors.New(messageConstants.REST_GENERIC_ERROR)
	}

	filter := bson.M{
		"$or": []bson.M{
			bson.M{fmt.Sprintf("%s.%s", fieldConstants.FIELD_ACCOUNT_MOVEMENTS, fieldConstants.FIELD_MOVEMENT_SENDER_IBAN): myAccount.Iban},
			bson.M{fmt.Sprintf("%s.%s", fieldConstants.FIELD_ACCOUNT_MOVEMENTS, fieldConstants.FIELD_MOVEMENT_RECIPIENT_IBAN): myAccount.Iban},
		},
	}

	result, err := findEntitiesByFields(constants.COLL_ACCOUNTS, filter)
	if err != nil {
		return nil, err
	}

	for result.Next(context.TODO()) {
		err := result.Decode(&account)
		if err != nil {
			loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al decodificar uno de los resultados de obtener las cuentas cuyo movimiento contenga el iban %s en la BBDD. Error: %s", myAccount.Iban, err)
		} else {
			for _, movement := range account.Movements {
				if movement.RecipientIban == myAccount.Iban || movement.SenderIban == myAccount.Iban {
					movements = append(movements, movement)
				}
			}
		}
	}

	return &movements, nil
}

//Inserts a movement
func (this MovementsModel) InsertMovement(userId primitive.ObjectID, accountId primitive.ObjectID, movement entities.Movement) (int64, error) {
	filter := bson.M{fieldConstants.FIELD_USER_ID: userId, fieldConstants.FIELD_ID: accountId}

	update := bson.M{
		"$push": bson.M{
			fieldConstants.FIELD_ACCOUNT_MOVEMENTS: movement,
		},
	}
	updated, err := updateEntityByFields(constants.COLL_ACCOUNTS, filter, update)

	return updated, err
}

//Cancels a movement
func (this MovementsModel) CancelMovement(userId primitive.ObjectID, accountId primitive.ObjectID, movementId int64) (*entities.Movement, error) {
	var movement *entities.Movement
	var accountsModel AccountsModel

	account, err := accountsModel.FindAccountByUserIdAndAccountId(userId, accountId, false)
	if err != nil || account == nil {
		return nil, errors.New(messageConstants.REST_GENERIC_ERROR)
	}

	for _, movementAux := range account.Movements {
		if movementAux.MovementId == movementId {
			movement = &movementAux
			break
		}
	}

	if movement == nil {
		return nil, errors.New(messageConstants.REST_GENERIC_ERROR)
	}

	if movement.Type == constants.MOVEMENT_DEPOSIT || movement.Type == constants.MOVEMENT_WITHDRAWALS {
		return nil, errors.New(messageConstants.REST_MOVEMENT_NOT_VALID)
	}

	if time.Since(movement.Date).Hours() > 24 {
		return nil, errors.New(messageConstants.REST_MOVEMENT_TIME_EXPIRED)
	}

	filter := bson.M{
		fieldConstants.FIELD_USER_ID: userId, fieldConstants.FIELD_ID: accountId, fmt.Sprintf("%s.%s", fieldConstants.FIELD_ACCOUNT_MOVEMENTS, fieldConstants.FIELD_MOVEMENT_ID): movementId}

	update := bson.M{
		"$set": bson.M{
			fmt.Sprintf("%s.$.%s", fieldConstants.FIELD_ACCOUNT_MOVEMENTS, fieldConstants.FIELD_MOVEMENT_IS_CANCELED): true},
	}
	updated, err := updateEntityByFields(constants.COLL_ACCOUNTS, filter, update)
	if err != nil || updated == 0 {
		return nil, errors.New(messageConstants.REST_GENERIC_ERROR)
	}

	return movement, nil
}
