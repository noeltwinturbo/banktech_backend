package restUtils

import (
	"banktech_backend/entities"
	"banktech_backend/utils/loggerUtils"
	"encoding/json"
	"fmt"
	"net/http"
)

//Sets the response status code and the body output
func SetOKResponseBody(w http.ResponseWriter, status int, body interface{}) {
	w.WriteHeader(status)

	json, err := json.Marshal(body)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al obtener el JSON de respuesta de la petición. Error: %s", err)
	} else {
		fmt.Fprintf(w, string(json))
	}
}

//Sets the response status code and the error message
func SetOKOResponseMessage(w http.ResponseWriter, status int, message string) {
	w.WriteHeader(status)

	var responseMessage entities.ResponseMessage
	fmt.Fprintf(w, responseMessage.ToJSON(message))
}

//Sets only the response status code
func SetEmptyResponse(w http.ResponseWriter, status int) {
	w.WriteHeader(status)
}

//Sets the content type in the whole responses
func GenerateCommonMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		handler.ServeHTTP(w, r)
	})
}
