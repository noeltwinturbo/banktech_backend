package models

import (
	"banktech_backend/entities"
	"banktech_backend/utils/constants"
	"banktech_backend/utils/constants/fieldConstants"
	"banktech_backend/utils/loggerUtils"
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"go.mongodb.org/mongo-driver/bson"
)

type AccountsModel struct {
}

//Finds accounts by userId
func (this AccountsModel) FindAccountsByUserId(userId primitive.ObjectID) (*[]entities.Account, error) {
	accounts := []entities.Account{}
	var account entities.Account

	result, err := findEntitiesByFields(constants.COLL_ACCOUNTS, bson.M{fieldConstants.FIELD_USER_ID: userId})
	if err != nil {
		return nil, err
	}

	for result.Next(context.TODO()) {
		err := result.Decode(&account)
		if err != nil {
			loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al decodificar uno de los resultados de obtener las cuentas del usuario %s en la BBDD. Error: %s", userId, err)
		} else {
			account.TotalAmount = this.GetTotalAmount(account.Iban)
			accounts = append(accounts, account)
		}
	}

	return &accounts, nil
}

//Finds account by userId
func (this AccountsModel) FindAccountByIban(iban string) (*entities.Account, error) {
	var account entities.Account

	result, err := findEntityByFields(constants.COLL_ACCOUNTS, bson.M{fieldConstants.FIELD_ACCOUNT_IBAN: iban})
	if err != nil || result == nil {
		return nil, err
	}

	err = result.Decode(&account)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al decodificar la cuenta bancaria con IBAN %s en la BBDD. Error: %s", iban, err)
		return nil, err
	}

	return &account, nil
}

//Finds account by userId and accountId
func (this AccountsModel) FindAccountByUserIdAndAccountId(userId primitive.ObjectID, accountId primitive.ObjectID, withTotal bool) (*entities.Account, error) {
	var account entities.Account

	result, err := findEntityByFields(constants.COLL_ACCOUNTS, bson.M{fieldConstants.FIELD_USER_ID: userId, fieldConstants.FIELD_ID: accountId})
	if err != nil || result == nil {
		return nil, err
	}

	err = result.Decode(&account)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al decodificar la cuenta bancaria con ID %s en la BBDD. Error: %s", accountId, err)
		return nil, err
	}

	if withTotal {
		account.TotalAmount = this.GetTotalAmount(account.Iban)
	}

	return &account, nil
}

//Finds account by accountId
func (this AccountsModel) FindAccountByAccountId(accountId primitive.ObjectID) (*entities.Account, error) {
	var account entities.Account

	result, err := findEntityByFields(constants.COLL_ACCOUNTS, bson.M{fieldConstants.FIELD_ID: accountId})
	if err != nil || result == nil {
		return nil, err
	}

	err = result.Decode(&account)
	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al decodificar la cuenta bancaria con ID %s en la BBDD. Error: %s", accountId, err)
		return nil, err
	}

	return &account, nil
}

//Counts accounts by userId
func (this AccountsModel) CountAccountsByIban(iban string) int64 {
	result, _ := countEntitiesByFields(constants.COLL_ACCOUNTS, bson.M{fieldConstants.FIELD_ACCOUNT_IBAN: iban})

	return result
}

//Upserts a new account
func (this AccountsModel) UpsertAccount(newAccount *entities.Account) (*entities.Account, error) {
	var account entities.Account

	result, err := findEntityByFields(constants.COLL_ACCOUNTS, bson.M{fieldConstants.FIELD_ACCOUNT_IBAN: newAccount.Iban})
	if err != nil {
		return nil, err
	}

	if result != nil {
		err = result.Decode(&account)
		if err != nil {
			loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al decodificar la cuenta bancaria con IBAN %s en la BBDD. Error: %s", newAccount.Iban, err)
			return nil, err
		}

		account.UserId = newAccount.UserId
		account.Alias = newAccount.Alias

		filter := bson.M{fieldConstants.FIELD_ACCOUNT_IBAN: account.Iban}
		update := bson.D{
			{"$set", bson.D{
				{fieldConstants.FIELD_USER_ID, account.UserId},
				{fieldConstants.FIELD_ACCOUNT_ALIAS, account.Alias},
			}},
		}
		updated, err := updateEntityByFields(constants.COLL_ACCOUNTS, filter, update)
		if err != nil || updated == 0 {
			return nil, err
		}

	} else {
		result, err := insertEntity(constants.COLL_ACCOUNTS, newAccount)
		if err != nil || result == nil {
			return nil, err
		}

		newAccount.AccountId = result.InsertedID.(primitive.ObjectID)
		return newAccount, nil
	}

	return &account, nil
}

//Updates an account alias
func (this AccountsModel) UpdateAccountAlias(userId primitive.ObjectID, accountId primitive.ObjectID, alias string) (int64, error) {
	filter := bson.M{fieldConstants.FIELD_USER_ID: userId, fieldConstants.FIELD_ID: accountId}

	update := bson.D{
		{"$set", bson.D{
			{fieldConstants.FIELD_ACCOUNT_ALIAS, alias},
		}},
	}

	updated, err := updateEntityByFields(constants.COLL_ACCOUNTS, filter, update)

	return updated, err
}

//Updates a accounts dni
func (this AccountsModel) UpdateAccountsDni(userId primitive.ObjectID, dni string) (int64, error) {
	filter := bson.M{fieldConstants.FIELD_USER_ID: userId}

	update := bson.D{
		{"$set", bson.D{
			{fieldConstants.FIELD_ACCOUNT_OWNER, dni},
		}},
	}

	updated, err := updateEntitiesByFields(constants.COLL_ACCOUNTS, filter, update)

	return updated, err
}

//Disassociates the account by userId and accountid
func (this AccountsModel) DisassociateAccountByUserIdAndAccountId(userId primitive.ObjectID, accountId primitive.ObjectID) (int64, error) {
	filter := bson.M{fieldConstants.FIELD_USER_ID: userId, fieldConstants.FIELD_ID: accountId}
	update := bson.D{
		{"$set", bson.D{
			{fieldConstants.FIELD_USER_ID, primitive.NilObjectID},
		}},
	}

	updated, err := updateEntityByFields(constants.COLL_ACCOUNTS, filter, update)

	return updated, err
}

//Disassociates the account by userId
func (this AccountsModel) DisassociateAccountsByUserId(userId primitive.ObjectID) (int64, error) {
	filter := bson.M{fieldConstants.FIELD_USER_ID: userId}
	update := bson.D{
		{"$set", bson.D{
			{fieldConstants.FIELD_USER_ID, primitive.NilObjectID},
		}},
	}

	updated, err := updateEntitiesByFields(constants.COLL_ACCOUNTS, filter, update)

	return updated, err
}

//Associates the user accounts by dni
func (this AccountsModel) AssociateAccounts(userId primitive.ObjectID, dni string) (int64, error) {
	filter := bson.M{fieldConstants.FIELD_ACCOUNT_OWNER: dni}
	update := bson.D{
		{"$set", bson.D{
			{fieldConstants.FIELD_USER_ID, userId},
		}},
	}

	updated, err := updateEntitiesByFields(constants.COLL_ACCOUNTS, filter, update)

	return updated, err
}

//Changes the accoun owner
func (this AccountsModel) ChangeAccountOwner(userId primitive.ObjectID, accountId primitive.ObjectID, newDni string) (int64, error) {
	var usersModel UsersModel
	newOwnerId := primitive.NilObjectID

	user, err := usersModel.FindUserByDni(newDni)
	if err == nil && user != nil {
		newOwnerId = user.UserId
	}

	filter := bson.M{fieldConstants.FIELD_USER_ID: userId, fieldConstants.FIELD_ID: accountId}

	update := bson.D{
		{"$set", bson.D{
			{fieldConstants.FIELD_ACCOUNT_OWNER, newDni},
			{fieldConstants.FIELD_USER_ID, newOwnerId},
		}},
	}

	updated, err := updateEntitiesByFields(constants.COLL_ACCOUNTS, filter, update)

	return updated, err
}

//Gets account total amount
func (this AccountsModel) GetTotalAmount(iban string) float32 {
	var account entities.Account
	var totalAmount float32

	filter := bson.M{
		"$or": []bson.M{
			bson.M{fmt.Sprintf("%s.%s", fieldConstants.FIELD_ACCOUNT_MOVEMENTS, fieldConstants.FIELD_MOVEMENT_SENDER_IBAN): iban},
			bson.M{fmt.Sprintf("%s.%s", fieldConstants.FIELD_ACCOUNT_MOVEMENTS, fieldConstants.FIELD_MOVEMENT_RECIPIENT_IBAN): iban},
		},
	}

	result, err := findEntitiesByFields(constants.COLL_ACCOUNTS, filter)
	if err != nil {
		return 0
	}

	totalAmount = 0
	for result.Next(context.TODO()) {
		err := result.Decode(&account)
		if err != nil {
			loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al decodificar uno de los resultados de obtener las cuentas cuyo movimiento contenga el iban %s en la BBDD. Error: %s", iban, err)
			return 0
		} else {
			for _, movement := range account.Movements {
				if !movement.IsCanceled {
					if movement.RecipientIban == iban {
						switch movement.Type {
						case constants.MOVEMENT_WITHDRAWALS:
							totalAmount -= movement.Amount
						case constants.MOVEMENT_DEPOSIT, constants.MOVEMENT_TRANSFER:
							totalAmount += movement.Amount
						}
					} else if movement.SenderIban == iban {
						totalAmount -= movement.Amount
					}
				}
			}
		}
	}

	return totalAmount
}
