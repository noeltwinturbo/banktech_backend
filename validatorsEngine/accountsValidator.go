package validatorsEngine

import (
	"banktech_backend/entities"
	"banktech_backend/models"
	"banktech_backend/utils/constants/messageConstants"
	"errors"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type AccountsValidator struct {
}

//Executes the Accounts input validation before insert into the DDBB
func (this AccountsValidator) ValidateInsertAccount(account *entities.Account) error {
	var validatorEngine ValidatorEngine

	err := validatorEngine.ValidateStruct(account)
	if err != nil {
		return err
	}

	err = validateIfAccountExists(account.Iban, account.Owner)
	if err != nil {
		return err
	}

	account.AccountId = primitive.NewObjectID()
	account.Movements = []entities.Movement{}
	account.Date = time.Now()

	return nil
}

//Checks if the bank account exists
func validateIfAccountExists(iban string, dni string) error {
	var accountsModel models.AccountsModel

	result, err := accountsModel.FindAccountByIban(iban)
	if err != nil {
		return errors.New(messageConstants.REST_GENERIC_ERROR)
	} else if result != nil && (result.UserId != primitive.NilObjectID || result.Owner != dni) {
		return errors.New(fmt.Sprintf(messageConstants.REST_ACCOUNT_DUPLICATE_IBAN, iban))
	}

	return nil
}
