package mailUtils

import (
	"banktech_backend/utils/constants"
	"banktech_backend/utils/constants/envConfigConstants"
	"banktech_backend/utils/constants/mailConstants"
	"banktech_backend/utils/loggerUtils"
	"fmt"
	"net/smtp"
)

//Sends a mail
func SendMail(to string, body string) error {
	msg := fmt.Sprintf(constants.EMAIL_STRUCT, to, mailConstants.EMAIL_SUBJECT, constants.EMAIL_STRUCT_MIME, body)

	auth := smtp.PlainAuth(
		"",
		constants.EMAIL_FROM,
		constants.EMAIL_PASSWORD,
		constants.EMAIL_SERVER_ADDR,
	)

	err := smtp.SendMail(
		fmt.Sprintf("%s%s", constants.EMAIL_SERVER_ADDR, constants.EMAIL_SERVER_PORT),
		auth,
		constants.EMAIL_FROM,
		[]string{to},
		[]byte(msg),
	)

	if err != nil {
		loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al intentar enviar el mail al correo %s. Error: %s", to, err)
		return err
	}

	if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
		loggerUtils.LOGGER_FINE.Printf("Email correctamente enviado al correo %s", to)
	}

	return nil
}
