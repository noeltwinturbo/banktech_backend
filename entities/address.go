package entities

type Address struct {
	Street  string `json:"street" bson:"street" validate:"required,min=3,max=75"`
	City    string `json:"city" bson:"city" validate:"required,min=3,max=50"`
	Country string `json:"country" bson:"country" validate:"required,min=3,max=50"`
	Zipcode string `json:"zipcode" bson:"zipcode" validate:"required,zipcode"`
}
