package constants

//App name constant
const APP_NAME = "banktech"

//Server configs constants
const (
	SERVER_ADDR          = ":8080"
	SERVER_READ_TIMEOUT  = 10
	SERVER_WRITE_TIMEOUT = 10
)

//MongoDB configs constants
const (
	MONGO_URI            = "mongodb://localhost:27017"
	MONGO_TIMEOUT        = 10
	MONGO_AUTH_MECHANISM = "SCRAM-SHA-1"
	MONGO_USER           = "banktechadmin"
	MONGO_PASSWORD       = "Abc123.."
	MONGO_DATABASE       = "banktechdb"
)

//MongoDB collections constants
const (
	COLL_USERS        = "users"
	COLL_USER_CONFIGS = "userConfigs"
	COLL_ACCOUNTS     = "accounts"
	COLL_ENV_CONFIGS  = "envConfigs"
	COLL_IDENTIFIERS  = "identifiers"
)

//Encrypt constants
const (
	KEY_FILE_NAME  = "banktech.key"
	CERT_FILE_NAME = "banktech.crt"
)

//Encrypted key var
var KEY = []byte("s6v9y$B&E)H+MbQeThWmZq4t7w!z%C*F")

//Reset password config constants
const (
	RANDOM_PASSWORD_LENGTH = 10
	RANDOM_PASSWORD_CHARS  = "ABCDEFGHIJKLMNOPQRSTUVWXYZ@#$%&abcdefghijklmnopqrstuvwxyz@#$%&123456789@#$%&"
)

//Token constants
const (
	TOKEN_NAME       = "user_token"
	TOKEN_EXPIRATION = 60
)

//Directories constants
const (
	RESOURCES_PATH = "resources/"
	LOGS_PATH      = "logs.log"
)

//Email configs constants
const (
	EMAIL_SERVER_ADDR = "smtp.gmail.com"
	EMAIL_SERVER_PORT = ":587"
	EMAIL_FROM        = "banktechou@gmail.com"
	EMAIL_PASSWORD    = "Abc123.."
	EMAIL_STRUCT      = "To: %s\r\nSubject: %s\r\n%s\r\n\r\n%s\r\n"
	EMAIL_STRUCT_MIME = "MIME-version: 1.0;\r\nContent-Type: text/html; charset=\"UTF-8\";"
)

//Validator type names constants
const (
	VAL_TYPE_REQUIRED      = "required"
	VAL_TYPE_MAX           = "max"
	VAL_TYPE_MIN           = "min"
	VAL_TYPE_GTE           = "gte"
	VAL_TYPE_LTE           = "lte"
	VAL_TYPE_NETFIELD      = "nefield"
	VAL_TYPE_EMAIL         = "email"
	VAL_TYPE_PASSWORD      = "password"
	VAL_TYPE_PHONE         = "phone"
	VAL_TYPE_ZIPCODE       = "zipcode"
	VAL_TYPE_DNI           = "dni"
	VAL_TYPE_BIRTHDAY      = "birthday"
	VAL_TYPE_IBAN          = "iban"
	VAL_TYPE_MOVEMENT_TYPE = "movementType"
)

//Movement types constants
const (
	MOVEMENT_DEPOSIT          = 1
	MOVEMENT_DEPOSIT_TEXT     = "Ingreso en efectivo"
	MOVEMENT_TRANSFER         = 2
	MOVEMENT_WITHDRAWALS      = 3
	MOVEMENT_WITHDRAWALS_TEXT = "Retirada en efectivo"
)
