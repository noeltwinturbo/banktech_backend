package controllers

import (
	"banktech_backend/handles"
	"banktech_backend/utils/loggerUtils"
	"net/http"

	"github.com/gorilla/mux"
)

//Inits EnvConfigs controllers
func loadEnvConfigsControllers(r *mux.Router) {
	loggerUtils.LOGGER_INFO.Printf("Iniciando EnvConfigs Controllers")

	r.HandleFunc("/envConfigs", handles.GetEnvConfigs).Methods(http.MethodGet)
	r.HandleFunc("/envConfigs", handles.PostEnvConfigs).Methods(http.MethodPost)
	r.HandleFunc("/envConfigs", handles.PutEnvConfigs).Methods(http.MethodPut)
	r.HandleFunc("/envConfigs/{configName:[A-Za-z_0-9]+}", handles.DeleteEnvConfigs).Methods(http.MethodDelete)
	r.HandleFunc("/envConfigs/reload", handles.PostEnvConfigsReload).Methods(http.MethodPost)
	r.HandleFunc("/envConfigs/logs", handles.GetEnvConfigsLogs).Methods(http.MethodGet)
}
