package entities

import (
	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Claims struct {
	UserId  primitive.ObjectID `json:"userId"`
	IsAdmin bool               `json:"isAdmin"`
	jwt.StandardClaims
}
