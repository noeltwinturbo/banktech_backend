package handles

import (
	"banktech_backend/entities"
	"banktech_backend/models"
	"banktech_backend/utils/constants"
	"banktech_backend/utils/constants/envConfigConstants"
	"banktech_backend/utils/constants/fieldConstants"
	"banktech_backend/utils/constants/mailConstants"
	"banktech_backend/utils/constants/messageConstants"
	"banktech_backend/utils/loggerUtils"
	"banktech_backend/utils/mailUtils"
	"banktech_backend/utils/restUtils"
	"banktech_backend/utils/tokenUtils"
	"banktech_backend/validatorsEngine"
	"banktech_backend/viewEntities"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/gorilla/mux"
)

//Do /accounts GET actions
func GetAccounts(w http.ResponseWriter, r *http.Request) {
	var accountsModel models.AccountsModel

	userId, _ := tokenUtils.ValidateToken(w, r)
	if userId != nil {
		accounts, err := accountsModel.FindAccountsByUserId(*userId)
		if err != nil {
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
		} else {
			restUtils.SetOKResponseBody(w, http.StatusOK, accounts)
		}
	}
}

//Do /account GET actions
func GetAccount(w http.ResponseWriter, r *http.Request) {
	var accountsModel models.AccountsModel

	accountIdRaw := mux.Vars(r)[fieldConstants.FIELD_ACCOUNT_ID]
	accountId, err := primitive.ObjectIDFromHex(accountIdRaw)
	if err != nil {
		restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
	} else {
		userId, _ := tokenUtils.ValidateToken(w, r)
		if userId != nil {
			account, err := accountsModel.FindAccountByUserIdAndAccountId(*userId, accountId, true)
			if err != nil || account == nil {
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			} else {
				restUtils.SetOKResponseBody(w, http.StatusOK, account)
			}
		}
	}
}

//Do /accounts POST actions
func PostAccounts(w http.ResponseWriter, r *http.Request) {
	var account entities.Account
	var accountsModels models.AccountsModel
	var usersModel models.UsersModel
	var accountsValidator validatorsEngine.AccountsValidator

	userId, _ := tokenUtils.ValidateToken(w, r)
	if userId != nil {
		err := json.NewDecoder(r.Body).Decode(&account)
		if err != nil {
			loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al deserializar el body de la petición. Error: %s", err)
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
		} else {
			account.UserId = *userId
			if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
				input, _ := json.Marshal(&account)
				loggerUtils.LOGGER_FINE.Printf("Body de entrada: %s", input)
			}
			err = accountsValidator.ValidateInsertAccount(&account)
			if err != nil {
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, err.Error())
			} else {
				newAccount, err := accountsModels.UpsertAccount(&account)
				if err != nil {
					restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
				} else {
					if userEmail, userName := usersModel.IsGeneralNotifyEnabled(*userId); userEmail != nil {
						mailUtils.SendMail(*userEmail, fmt.Sprintf(mailConstants.EMAIL_ACCOUNT_CREATED, *userName, account.Iban))
					}
					restUtils.SetOKResponseBody(w, http.StatusOK, newAccount)
				}
			}
		}
	}
}

//Do /accounts/accountId PUT actions
func PutAccounts(w http.ResponseWriter, r *http.Request) {
	var accountAliasView viewEntities.AccountAliasView
	var accountsModel models.AccountsModel
	var validatorEngine validatorsEngine.ValidatorEngine

	accountIdRaw := mux.Vars(r)[fieldConstants.FIELD_ACCOUNT_ID]
	accountId, err := primitive.ObjectIDFromHex(accountIdRaw)
	if err != nil {
		restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
	} else {
		userId, _ := tokenUtils.ValidateToken(w, r)
		if userId != nil {
			err := json.NewDecoder(r.Body).Decode(&accountAliasView)
			if err != nil {
				loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al deserializar el body de la petición. Error: %s", err)
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			} else {
				err = validatorEngine.ValidateStruct(accountAliasView)
				if err != nil {
					restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, err.Error())
				} else {
					updated, err := accountsModel.UpdateAccountAlias(*userId, accountId, accountAliasView.Alias)
					if err != nil || updated == 0 {
						restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
					} else {
						restUtils.SetOKOResponseMessage(w, http.StatusOK, messageConstants.REST_ACCOUNT_ALIAS_UPDATED)
					}
				}
			}
		}
	}
}

//Do /accounts/accountId DELETE actions
func DeleteAccounts(w http.ResponseWriter, r *http.Request) {
	var accountsModel models.AccountsModel
	var usersModel models.UsersModel

	accountIdRaw := mux.Vars(r)[fieldConstants.FIELD_ACCOUNT_ID]
	accountId, err := primitive.ObjectIDFromHex(accountIdRaw)
	if err != nil {
		restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
	} else {
		userId, _ := tokenUtils.ValidateToken(w, r)
		if userId != nil {
			deleted, err := accountsModel.DisassociateAccountByUserIdAndAccountId(*userId, accountId)
			if err != nil || deleted == 0 {
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			} else {
				if userEmail, userName := usersModel.IsGeneralNotifyEnabled(*userId); userEmail != nil {
					account, _ := accountsModel.FindAccountByAccountId(accountId)
					mailUtils.SendMail(*userEmail, fmt.Sprintf(mailConstants.EMAIL_ACCOUNT_DELETED, *userName, account.Iban))
				}
				restUtils.SetOKOResponseMessage(w, http.StatusOK, messageConstants.REST_ACCOUNT_DELETED)
			}
		}
	}
}

//Do /accounts/accountId/changeOwner POST actions
func PutAccountChangeOwner(w http.ResponseWriter, r *http.Request) {
	var accountNewOwnerView viewEntities.AccountNewOwnerView
	var accountsModel models.AccountsModel
	var usersModel models.UsersModel
	var validatorEngine validatorsEngine.ValidatorEngine

	accountIdRaw := mux.Vars(r)[fieldConstants.FIELD_ACCOUNT_ID]
	accountId, err := primitive.ObjectIDFromHex(accountIdRaw)
	if err != nil {
		restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
	} else {
		userId, _ := tokenUtils.ValidateToken(w, r)
		if userId != nil {
			err := json.NewDecoder(r.Body).Decode(&accountNewOwnerView)
			if err != nil {
				loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al deserializar el body de la petición. Error: %s", err)
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			} else {
				err = validatorEngine.ValidateStruct(accountNewOwnerView)
				if err != nil {
					restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, err.Error())
				} else {
					updated, err := accountsModel.ChangeAccountOwner(*userId, accountId, accountNewOwnerView.Dni)
					if err != nil || updated == 0 {
						restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
					} else {
						if userEmail, userName := usersModel.IsGeneralNotifyEnabled(*userId); userEmail != nil {
							account, _ := accountsModel.FindAccountByAccountId(accountId)
							mailUtils.SendMail(*userEmail, fmt.Sprintf(mailConstants.EMAIL_ACCOUNT_OWNER_CHANGED, *userName, account.Iban, accountNewOwnerView.Dni))
						}
						restUtils.SetOKOResponseMessage(w, http.StatusOK, messageConstants.REST_ACCOUNT_OWNER_CHANGED)
					}
				}
			}
		}
	}
}

//Do /accounts/accountId/movements GET actions
func GetAccountMovements(w http.ResponseWriter, r *http.Request) {
	var movementsModel models.MovementsModel

	accountIdRaw := mux.Vars(r)[fieldConstants.FIELD_ACCOUNT_ID]
	accountId, err := primitive.ObjectIDFromHex(accountIdRaw)
	if err != nil {
		restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
	} else {
		userId, _ := tokenUtils.ValidateToken(w, r)
		if userId != nil {
			movements, err := movementsModel.FindMovements(*userId, accountId)
			if err != nil {
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			} else {
				restUtils.SetOKResponseBody(w, http.StatusOK, movements)
			}
		}
	}
}

//Do /accounts/accountId/movements POST actions
func PostAccountMovements(w http.ResponseWriter, r *http.Request) {
	var movement entities.Movement
	var movementsModel models.MovementsModel
	var usersModel models.UsersModel
	var movementsValidator validatorsEngine.MovementsValidator

	accountIdRaw := mux.Vars(r)[fieldConstants.FIELD_ACCOUNT_ID]
	accountId, err := primitive.ObjectIDFromHex(accountIdRaw)
	if err != nil {
		restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
	} else {
		userId, _ := tokenUtils.ValidateToken(w, r)
		if userId != nil {
			err := json.NewDecoder(r.Body).Decode(&movement)
			if err != nil {
				loggerUtils.LOGGER_ERROR.Printf("Se ha producido un error al deserializar el body de la petición. Error: %s", err)
				restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
			} else {
				if *envConfigConstants.IS_LOGGER_FINE_ENABLE {
					input, _ := json.Marshal(&movement)
					loggerUtils.LOGGER_FINE.Printf("Body de entrada: %s", input)
				}
				err = movementsValidator.ValidateInsertMovement(&movement, accountId)
				if err != nil {
					restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, err.Error())
				} else {
					inserted, err := movementsModel.InsertMovement(*userId, accountId, movement)
					if err != nil || inserted == 0 {
						restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
					} else {
						if userEmail, userName := usersModel.IsMovementsNotifyEnabled(*userId, movement.Amount); userEmail != nil {
							switch movement.Type {
							case constants.MOVEMENT_DEPOSIT:
								mailUtils.SendMail(*userEmail, fmt.Sprintf(mailConstants.EMAIL_MOVEMENT_DONE_1, *userName, movement.SenderIban, fmt.Sprintf("%.2f", movement.Amount)))
							case constants.MOVEMENT_TRANSFER:
								mailUtils.SendMail(*userEmail, fmt.Sprintf(mailConstants.EMAIL_MOVEMENT_DONE_2, *userName, movement.SenderIban, movement.Concept, fmt.Sprintf("%.2f", movement.Amount)))
							case constants.MOVEMENT_WITHDRAWALS:
								mailUtils.SendMail(*userEmail, fmt.Sprintf(mailConstants.EMAIL_MOVEMENT_DONE_3, *userName, movement.SenderIban, fmt.Sprintf("%.2f", movement.Amount)))
							}
						}
						restUtils.SetOKResponseBody(w, http.StatusOK, movement)
					}
				}
			}
		}
	}
}

//Do /accounts/accountId/movements/movementId DELETE actions
func DeleteAccountMovements(w http.ResponseWriter, r *http.Request) {
	var movementsModel models.MovementsModel
	var usersModel models.UsersModel
	var accountsModel models.AccountsModel

	accountIdRaw := mux.Vars(r)[fieldConstants.FIELD_ACCOUNT_ID]
	accountId, err := primitive.ObjectIDFromHex(accountIdRaw)
	if err != nil {
		restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
	} else {
		movementIdRaw := mux.Vars(r)[fieldConstants.FIELD_MOVEMENT_ID]
		movementId, err := strconv.Atoi(movementIdRaw)
		if err != nil {
			restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, messageConstants.REST_GENERIC_ERROR)
		} else {
			userId, _ := tokenUtils.ValidateToken(w, r)
			if userId != nil {
				movement, err := movementsModel.CancelMovement(*userId, accountId, int64(movementId))
				if err != nil {
					restUtils.SetOKOResponseMessage(w, http.StatusBadRequest, err.Error())
				} else {
					if userEmail, userName := usersModel.IsGeneralNotifyEnabled(*userId); userEmail != nil {
						account, _ := accountsModel.FindAccountByAccountId(accountId)
						mailUtils.SendMail(*userEmail, fmt.Sprintf(mailConstants.EMAIL_MOVEMENT_CANCELED, *userName, account.Iban, movement.Concept, fmt.Sprintf("%.2f", movement.Amount)))
					}
					restUtils.SetOKOResponseMessage(w, http.StatusOK, messageConstants.REST_MOVEMENT_CANCELED)
				}
			}
		}
	}
}
