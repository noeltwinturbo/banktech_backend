package entities

type UserConfig struct {
	IsGeneralNotifyEnable   bool  `json:"isGeneralNotifyEnable" bson:"isGeneralNotifyEnable"`
	IsMovementsNotifyEnable bool  `json:"isMovementsNotifyEnable" bson:"isMovementsNotifyEnable"`
	MinMovAmntNotify        int64 `json:"minMovAmntNotify" bson:"minMovAmntNotify" validate:"gte=0,lte=1000"`
}
