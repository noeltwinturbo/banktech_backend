package mailConstants

//Mail subject constants
const EMAIL_SUBJECT = ".:BANKTECH:."

//Mails body constants
const (
	EMAIL_USER_PASSWORD_RESET   = "<html><body><h3>Contraseña reseteada</h3><p>Hola %s! La contraseña de tu cuenta ha sido reseteada.</p><p>Tu nueva contraseña es <b>%s</b>, no olvides cambiarla.</p></body></html>"
	EMAIL_USER_CREATED          = "<html><body><h3>Bienvenido %s</h3><p>Te damos la bienvenida desde el equipo de banktech.</p><p>A partir de ahora puedes empezar a utilizar todos nuestros servicios.</p></body></html>"
	EMAIL_USER_DELETED          = "<html><body><h3>Cuenta eliminada</h3><p>Hola %s! Tu cuenta ha sido eliminada, lamentamos que tengas que irte...</p></body></html>"
	EMAIL_ACCOUNT_CREATED       = "<html><body><h3>Cuenta bancaria añadida</h3><p>Hola %s! Has añdido una nueva cuenta bancaria con IBAN %s.</p></body></html>"
	EMAIL_ACCOUNT_OWNER_CHANGED = "<html><body><h3>Cuenta bancaria cambio titular</h3><p>Hola %s! Has cambiado de titular la cuenta bancaria con IBAN %s a la persona cuyo DNI es %s.</p></body></html>"
	EMAIL_ACCOUNT_DELETED       = "<html><body><h3>Cuenta bancaria eliminada</h3><p>Hola %s! Has quitado la cuenta bancaria con IBAN %s.</p><p>Podrás volver a añadirla siempre que quieras.</p></body></html>"
	EMAIL_MOVEMENT_DONE_1       = "<html><body><h3>Movimiento realizado</h3><p>Hola %s! Se ha realizado un ingreso en efectivo a la cuenta con IBAN: %s cuya cuantía fue %s€.</p></body></html>"
	EMAIL_MOVEMENT_DONE_2       = "<html><body><h3>Movimiento realizado</h3><p>Hola %s! Se ha realizado una transferencia desde la cuenta con IBAN: %s cuyo concepto es: %s y el movimiento fue: %s€.</p></body></html>"
	EMAIL_MOVEMENT_DONE_3       = "<html><body><h3>Movimiento realizado</h3><p>Hola %s! Se ha realizado una retirada en efectivo desde la cuenta con IBAN: %s cuya cuantía fue %s€.</p></body></html>"
	EMAIL_MOVEMENT_CANCELED     = "<html><body><h3>Movimiento cancelado</h3><p>Hola %s! Has cancelado el movimiento de la cuenta con IBAN: %s cuyo concepto es: %s y el movimiento fue: %s€.</p></body></html>"
)
