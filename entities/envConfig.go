package entities

type EnvConfig struct {
	ConfigName  string `json:"configName" bson:"configName" validate:"required,min=3,max=50"`
	ConfigValue string `json:"configValue" bson:"configValue" validate:"required,max=100"`
}
